import React, {useEffect, useState} from 'react';
import {

    Text,
    TouchableOpacity,
    View,

    Modal, FlatList, Image,ScrollView
} from 'react-native';
import styles from '../assets/styles';
import Form from './form';

const ModalUser = (props) => {
    console.log('all colection props', props);

    return (
        <Modal
            animationType="slide"

            visible={props.user}


        >
            <ScrollView>

            <View style={styles.centeredView}>

                <View style={styles.modalUser}>
                    <Text style={{
                        color: '#000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',
                    }}>ADD YOUR DETAILS</Text>

                </View>


                <Form {...props}/>



            </View>
            </ScrollView>


        </Modal>
    );
};

export default ModalUser;
