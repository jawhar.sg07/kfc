import React, {Component} from 'react';
import { View, TouchableOpacity, Text, FlatList, StyleSheet, LayoutAnimation, Platform, UIManager} from "react-native";
import { Colors } from '../config/color';
import Icon from "react-native-vector-icons/MaterialIcons";
import Up from '../assets/images/upBlack.svg'
export default class Accordian extends Component{

    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            expanded : false,
        }


    }

    render() {

        return (
            <View style={{backgroundColor:'white', shadowColor: '#000',
                shadowOffset: {width: 10, height: 2},
                shadowOpacity: 1.5,
                shadowRadius: 2, elevation:!this.state.expanded?0:5,  marginHorizontal:10,marginVertical:5}}>
                <TouchableOpacity  style={styles.row(this.state.expanded)} onPress={()=>this.toggleExpand()}>
                    <View style={{flex:0.95}}>
                    <Text style={styles.title}>{this.props.title}</Text>
                    </View>
                    <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={Colors.DARKGRAY} />
                </TouchableOpacity>

                {
                    this.state.expanded &&
                    <View style={styles.textStyle}>
                        <Text style={{color:'#726F6F',fontFamily:'Montserrat-Regular',fontSize:13,  lineHeight:20,}}>{this.props.data}</Text>
                    </View>
                }

            </View>
        )
    }

    onClick=(index)=>{
        const temp = this.state.data.slice()
        temp[index].value = !temp[index].value
        this.setState({data: temp})
    }

    toggleExpand=()=>{
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({expanded : !this.state.expanded})
    }

}

const styles = StyleSheet.create({
    container:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle:{
      marginHorizontal:20,
        marginBottom:10

    },
    button:{
       flex:1,
        alignItems:'center',

        fontSize: 12,
    },
    title:{
        fontSize: 14,
        fontWeight:'bold',
        color: '#000000',
        padding:10,
        fontFamily:'Montserrat-Bold',
        lineHeight:20
    },
    itemActive:{
        fontSize: 12,
        color: Colors.GREEN,
    },
    itemInActive:{
        fontSize: 12,
        color: Colors.DARKGRAY,
    },
    btnActive:{
        borderColor: Colors.GREEN,
    },
    btnInActive:{
        borderColor: Colors.DARKGRAY,
    },
    row:(expanded)=>({
        flex:1,
        backgroundColor: 'white',

        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-between',
        shadowColor: '#000',
        shadowOffset: {width: 10, height: 2},
        shadowOpacity: 1.5,
        shadowRadius: 2,
        elevation: !expanded?5:0,
        marginBottom:10,
        height:70,
        borderRadius:4
    }),
    childRow:{
        flexDirection: 'row',
        justifyContent:'space-between',
        backgroundColor: Colors.GRAY,
    },
    parentHr:{
        height:1,
        color: Colors.WHITE,
        width:'100%'
    },
    childHr:{
        height:1,
        backgroundColor: Colors.LIGHTGRAY,
        width:'100%',
    },
    colorActive:{
        borderColor: Colors.GREEN,
    },
    colorInActive:{
        borderColor: Colors.DARKGRAY,
    }

});
