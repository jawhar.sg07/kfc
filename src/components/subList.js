import React, {useEffect, useState} from 'react';
import {
    Dimensions,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    Image,
    ScrollView,
} from 'react-native';
import styles from '../assets/styles';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const Header=(props,index)=>{
    console.log('indexx',props);
    console.log('indexxxxx',index);
    return(
        <View>
            <Image source={require('../assets/images/img1.png')} style={{width: wp('100%'), height: hp('30%')}}/>
            <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>

                <View style={{display: 'flex', justifyContent: 'flex-end'}}>
                    <Text style={{
                        color: 'black',
                        fontSize: 18,
                        paddingHorizontal: 15,
                        fontFamily: 'Montserrat-Black',
                    }}>MEALS {props.menu[props.navigation.state.params.index]?.name}</Text>
                </View>
                <View style={styles.threeTextContainer}>
                    <View style={styles.boxSimple}>
                    </View>
                    <View style={styles.boxSimple}>
                    </View>
                    <View style={styles.boxSimple}>
                    </View>

                </View>
            </View>
        </View>
    )
}

const SubList=(props)=>{
    console.log('propssss nextttttttt',props);



    return(


            <FlatList
                data={props.navigation.state.params.item.meals}
                renderItem={({ item,index }) => (

                        <TouchableOpacity onPress={()=>{
                            props.navigation.navigate('OrderDetails', {item});
                        }}   style={styles.btnListDetails}>
                            <View style={{
                                display: 'flex',
                                flexDirection: 'column',
                                flex: 0.6,
                                marginHorizontal: 10,
                                marginVertical: 15,
                            }}>
                                <Text
                                    style={{
                                        color: '#E82322',
                                        fontSize: 14,
                                        paddingHorizontal: 15,
                                        fontFamily: 'Montserrat-Bold',
                                    }}>{item.name}</Text>

                                <Text
                                    style={{
                                        color: 'black',
                                        fontSize: 13,
                                        paddingHorizontal: 15,
                                        fontFamily: 'Montserrat-Bold',
                                        marginVertical: 5,
                                    }}>{Number(item.price).toFixed(3)} DT</Text>
                                <Text
                                    style={{
                                        color: '#474545',
                                        fontSize: 12,
                                        paddingHorizontal: 15,
                                        fontFamily: 'Montserrat-Regular',
                                        lineHeight:20
                                    }}>{item.description}</Text>
                            </View>
                            <View style={{display: 'flex', flex: 0.4, justifyContent: 'center'}}>
                                <Image source={item.img} style={{width: wp('35%'), height: hp('20%')}}/>
                            </View>


                        </TouchableOpacity>

                )}
                //Setting the number of column

                keyExtractor={(item, index) => item.name.toString()}
                ListHeaderComponent={(item)=>Header(props,item)}
            />


    )

}
export default SubList
