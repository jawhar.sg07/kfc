import React, {useEffect, useState} from 'react';
import {
    Dimensions,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    Image,
    ScrollView,
    TouchableHighlight,
    Modal,
    TextInput,
} from 'react-native';
import styles from '../assets/styles';
import Back from '../assets/images/back.svg';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Pin from '../assets/images/pin.svg';
import Next from '../assets/images/next.svg';
import ModalReacapChoice from './ModalRecapChoice';

const ModalOrderRestaurantDetails = (props) => {
    console.log('props details',props);
    let orders = [
        {id: 1, name: 'GARDINER LN, LOUISVILLE'},
        {id: 2, name: 'BOXE MEALS'},
        {id: 3, name: 'BURGERS'},
        {id: 4, name: 'BUCKETS'},

        {id: 5, name: 'CHICKEN MEALS'}, {id: 5, name: 'CHICKEN MEALS'},
        {id: 5, name: 'CHICKEN MEALS'},

    ];
    return (
        <Modal
            animationType="slide"

            visible={props.modalOrder}


        >
            <View style={styles.centeredView}>

                <View style={{flex: 0.25}}>
                    <View style={styles.modalView}>
                        <TouchableOpacity onPress={() => {
                            props.openModalChoose();
                            props.closeModalOrder();
                        }}>
                            <Back/>
                        </TouchableOpacity>

                    </View>
                    <View style={{display: 'flex', alignItems: 'center', flexDirection: 'column'}}>
                        <Text style={{
                            color: 'black',
                            fontSize: 16,
                            fontFamily: 'Montserrat-Black',
                            marginBottom: 5,
                        }}>TELL US HOW YOU'D LIKE IT</Text>
                        <Text style={{
                            color: '#474545',
                            fontSize: 12,
                            fontFamily: 'Montserrat-Regular',
                        }}>Find a KFC, using your location</Text>
                    </View>
                </View>
                <View style={{display: 'flex', flex: 0.75, flexDirection: 'column'}}>
                    <View style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginHorizontal: 15,
                    }}>
                        <TextInput
                            placeholder='Use your post code'
                            style={{borderColor: 'gray', backgroundColor: '#F5F5F5', width: wp('75%')}}
                            onChangeText={text => onChangeText(text)}

                        />
                        <TouchableOpacity onPress={()=>props.openModalMap()}
                            style={{alignItems: 'center', justifyContent: 'center'}}

                        >
                            <Pin/>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1}}>
                        <FlatList
                            data={props.place}
                            renderItem={({item}) => (
                                <View style={{flexDirection: 'column', margin: 1}}>
                                    <TouchableOpacity onPress={()=>{
                                        props.savePoint(item)
                                        props.closeModalOrder();
                                        props.openModalRecap();

                                    }} style={styles.orderContainerBtn}>

                                        <View style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            flex: 1,
                                            alignItems: 'center',
                                            marginBottom: 10,
                                        }}>
                                            <Text style={{
                                                color: 'black',
                                                fontSize: 14,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>{item.label}</Text>
                                            <Next/>
                                        </View>
                                        <View style={{
                                            display: 'flex',
                                            flexDirection: 'column',
                                            flex: 1,


                                        }}>
                                            <View style={{display: 'flex',marginBottom:5}}>
                                                <Text style={{
                                                    color: '#949494',
                                                    fontSize: 12,
                                                    fontFamily: 'Montserrat-Regular',
                                                }}>{item.description}</Text>
                                            </View>
                                            <View style={{display:'flex',flexDirection:'row'}}>
                                         <Text>Temps De Travail</Text>
                                            <Text>  {item.openTime} Jusqu'à </Text>
                                            <Text>{item.closeTime}</Text>
                                            </View>
                                        </View>


                                    </TouchableOpacity>
                                </View>
                            )}
                            //Setting the number of column

                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>

                    <View style={{
                        display: 'flex',
                        justifyContent: 'flex-end',

                        alignItems: 'center',
                    }}>
                        <View style={styles.threeTextContainer}>
                            <View style={styles.boxSimple}>
                            </View>
                            <View style={styles.boxSimple}>
                            </View>
                            <View style={styles.boxSimple}>
                            </View>

                        </View>
                    </View>
                </View>
            </View>
        </Modal>
    );
};
export default ModalOrderRestaurantDetails;
