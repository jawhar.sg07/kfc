import React, {useEffect, useState} from 'react';
import {

    Text,
    TouchableOpacity,
    View,

    Modal, FlatList, Image,
} from 'react-native';
import styles from '../assets/styles';
import PaymentLists from './PaymentLists';
import Radio from '../assets/images/radioCheck.svg';
import Circle from '../assets/images/radio.svg';
import Next from '../assets/images/next.svg';

const ChooseCollection = (props) => {

    useEffect(() => {
        props.getCollections();
    }, []);
    return (
        <Modal
            animationType="slide"

            visible={props.collection}


        >
            <View style={styles.centeredView}>

                <View style={styles.modalCollection}>
                    <Text style={{
                        color: '#000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',
                    }}>CHOOSE A COLLECTION POINT</Text>

                </View>


                <View style={{flex:0.7}}>
                    <FlatList
                        data={props.collections}
                        renderItem={({item, index}) => {
                            return (

                                <TouchableOpacity onPress={()=>{
                                    props.savePoint(item)
                                    props.openEat();
                                }} style={styles.touchableCollection}>
                                    <View style={{
                                        display: 'flex',
                                        flexDirection: 'row',
                                        padding: 10,
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'space-between',


                                    }}>

                                        <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                                            <Image source={item.image}/>

                                            <Text
                                                style={{
                                                    color: '#000000',
                                                    fontSize: 14,
                                                    marginHorizontal: 10,
                                                    fontFamily: 'Montserrat-Bold',

                                                }}>{item.label}</Text>

                                        </View>
                                        <Next/>

                                    </View>


                                </TouchableOpacity>


                            );
                        }}
                        keyExtractor={item => item.label.toString()}
                    />
                </View>


                <TouchableOpacity onPress={() => {
                    props.closeCollection();

                }} style={{
                    alignItems: 'center',
                    backgroundColor: 'white',
                    borderRadius: 50,
                    padding: 16,
                    borderColor: 'black',
                    borderWidth: 1,
                    marginHorizontal: 10,
                }}

                >
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 15,
                            fontFamily: 'Montserrat-Bold',

                        }}

                    >CANCEL</Text>
                </TouchableOpacity>
            </View>


        </Modal>
    );
};

export default ChooseCollection;
