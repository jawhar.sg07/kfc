import React, {useEffect, useState} from 'react';
import {View, Button, StyleSheet, Text, TouchableOpacity, Alert} from 'react-native';

import { TextInput } from 'react-native-paper';

import {CheckBox} from 'react-native-elements';

const FormPersonal = (props) => {
    console.log('props form', props);
    const [sms, setSms] = useState(false)
    const [email, setEmail] = useState(false)
    const [condition, setCondition] = useState(false)
    const payload={};

    return (
        <View style={styles.root}>
            <View style={{marginHorizontal: 15}}>
                <TextInput
                    label="Nom"
                    style={styles.input}
                    underlineColor='transparent'
                    text='black'
                    mode='flat'
                    theme={{
                        colors: {
                            placeholder: 'black', text: 'black', primary: 'black',
                            underlineColor: 'red', background: '#F5F5F5',
                        },
                        fonts: { regular: 'Montserrat-Bold' }

                    }}
                    fontFamily={"Montserrat-Bold"}
                    onChangeText={(text)=>payload.firstName=text}
                />
                <TextInput
                    label="Prénom"
                    style={styles.input}
                    underlineColor='transparent'
                    text='black'
                    mode='flat'
                    theme={{
                        colors: {
                            placeholder: 'black', text: 'black', primary: 'black',
                            underlineColor: 'red', background: '#F5F5F5'
                        },
                        fonts: { regular: 'Montserrat-Bold' }

                    }}
                    fontFamily={"Montserrat-Bold"}
                    onChangeText={(text)=>payload.lastName=text}

                />
                <TextInput
                    label="Numéro téléphone "
                    style={styles.input}
                    underlineColor='transparent'
                    text='black'
                    mode='flat'
                    theme={{
                        colors: {
                            placeholder: 'black', text: 'black', primary: 'black',
                            underlineColor: 'red', background: '#F5F5F5'
                        },
                        fonts: { regular: 'Montserrat-Bold' }

                    }}
                    fontFamily={"Montserrat-Bold"}


                    onChangeText={(text)=>payload.phoneNumber=text}

                />
                <TextInput
                    label="Adresse mail"
                    style={styles.input}
                    underlineColor='transparent'
                    text='black'
                    mode='flat'
                    theme={{
                        colors: {
                            placeholder: 'black', text: 'black', primary: 'black',
                            underlineColor: 'red', background: '#F5F5F5'
                        },
                        fonts: { regular: 'Montserrat-Bold' }

                    }}
                    fontFamily={"Montserrat-Bold"}

                    onChangeText={(text)=>payload.email=text}


                />
                <TextInput
                    secureTextEntry={true}
                    label="Mot de passe"
                    style={styles.input}
                    underlineColor='transparent'
                    text='black'
                    mode='flat'
                    theme={{
                        colors: {
                            placeholder: 'black', text: 'black', primary: 'black',
                            underlineColor: 'red', background: '#F5F5F5'
                        },
                        fonts: { regular: 'Montserrat-Bold' }

                    }}
                    fontFamily={"Montserrat-Bold"}

                    onChangeText={(text)=>payload.password=text}


                />

            </View>
            <View>
                <CheckBox
                    size={35}
                    containerStyle={{backgroundColor:'white',borderWidth:0,padding:0,marginVertical:20}}
                    checked={sms}
                    onPress={() => setSms(!sms)}
                    checkedColor={'#000000'}
                    title='Recevoir les offres de nos partenaires'
                />
                <CheckBox
                    size={35}
                    checked={email}
                    containerStyle={{backgroundColor:'white',borderWidth:0,padding:0}}
                    onPress={() => setEmail(!email)}
                    checkedColor={'#000000'}
                    title='Recevoir notre newsletter Vous pouvez vous désinscrire à n importe quel moment. Pour cela veuillez trouver nos coordonnées dans les mentions légales.'
                />

            </View>
            <TouchableOpacity onPress={() => {
               Alert.alert('not yet !!')
            }} style={{
                alignItems: 'center',
                backgroundColor: '#E3002B',
                borderRadius: 50,
                padding: 16,
                marginVertical: 20,
                marginHorizontal: 10,
            }}

            >
                <Text
                    style={{
                        color: 'white',
                        fontSize: 15,
                        fontFamily: 'Montserrat-Bold',
                    }}

                >ENREGISTER</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
                props.navigation.goBack()

            }} style={{
                alignItems: 'center',
                backgroundColor: 'white',
                borderRadius: 50,
                padding: 16,
                borderColor: 'black',
                borderWidth: 1,
                marginHorizontal: 10,
            }}

            >
                <Text
                    style={{
                        color: 'black',
                        fontSize: 15,
                        fontFamily: 'Montserrat-Bold',

                    }}

                >CANCEL</Text>
            </TouchableOpacity>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkboxContainer: {
        flexDirection: 'row',
    },
    checkbox: {
        alignSelf: 'center',
    },
    label: {
        margin: 8,
    },
    root: {
        justifyContent: 'center',
    },
    input: {

        marginBottom: 8,
        borderColor: '#F5F5F5',
        borderWidth: 1,
        backgroundColor: '#F5F5F5',
        fontFamily:'Montserrat-Bold',
        fontSize:15
    },
});
export default FormPersonal;
