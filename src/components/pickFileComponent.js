import React, {useEffect, useState} from 'react';
import {Animated, FlatList, Image, Text, TouchableOpacity, View,StyleSheet,ScrollView,TextInput} from 'react-native';
import DocumentPicker from 'react-native-document-picker';

export default class PickFile extends React.Component {
    constructor(props) {
        super(props);
        //Initialization of the state to store the selected file related attribute
        this.state = {
            singleFile: '',
            multipleFile: [],
        };
    }
    async selectOneFile() {
        //Opening Document Picker for selection of one file
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
                //There can me more options as well
                // DocumentPicker.types.allFiles
                // DocumentPicker.types.images
                // DocumentPicker.types.plainText
                // DocumentPicker.types.audio
                // DocumentPicker.types.pdf
            });
            //Printing the log realted to the file
            console.log('res : ' + JSON.stringify(res));
            console.log('URI : ' + res.uri);
            console.log('Type : ' + res.type);
            console.log('File Name : ' + res.name);
            console.log('File Size : ' + res.size);
            //Setting the state to show single file attributes
            this.setState({ singleFile: res });
        } catch (err) {
            //Handling any exception (If any)
            if (DocumentPicker.isCancel(err)) {
                //If user canceled the document selection
                alert('Canceled from single doc picker');
            } else {
                //For Unknown Error
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    }

    async selectMultipleFile() {
        //Opening Document Picker for selection of multiple file
        try {
            const results = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.images],
                //There can me more options as well find above
            });
            for (const res of results) {
                //Printing the log realted to the file
                console.log('res : ' + JSON.stringify(res));
                console.log('URI : ' + res.uri);
                console.log('Type : ' + res.type);
                console.log('File Name : ' + res.name);
                console.log('File Size : ' + res.size);
            }
            //Setting the state to show multiple file attributes
            this.setState({ multipleFile: results });
        } catch (err) {
            //Handling any exception (If any)
            if (DocumentPicker.isCancel(err)) {
                //If user canceled the document selection
                alert('Canceled from multiple doc picker');
            } else {
                //For Unknown Error
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    }

    render() {
        return (
            <View style={styles.containerFile}>
                <TextInput
                    style={{ height: 50, borderColor: '#00000029', borderWidth: 1,width:200,backgroundColor:'#f6f6f6' }}
                    editable={false}

                    value= {this.state.singleFile.name ? this.state.singleFile.name : ''}
                />
                {/*To show single file attribute*/}
                <TouchableOpacity

                    style={styles.buttonFile}
                    onPress={this.selectOneFile.bind(this)}>
                    {/*Single file selection button*/}
                    <Text style={{color:'white',fontFamily:'Montserrat-Regular',fontSize:10}}>
                       CHOISIR UN FICHIER
                    </Text>

                </TouchableOpacity>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerFile: {
        flex: 1,

        marginHorizontal:15,
        marginTop: 10,
        marginBottom:30,
        display:'flex',
        flexDirection: 'row'
    },
    textStyle: {
        backgroundColor: '#fff',
        fontSize: 15,
        marginTop: 16,
        color: 'black',
    },
    buttonFile: {
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#000',
        padding: 15,
    },
    imageIconStyle: {
        height: 20,
        width: 20,
        resizeMode: 'stretch',
    },
});
