import React, {useEffect, useState} from 'react';
import {Dimensions, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, FlatList, Image,ScrollView} from 'react-native';


import styles from '../assets/styles';
import {calculateTotal} from '../actions/data';

const MenuBar=(props)=>{
useEffect(()=>{
    props.calculateTotal(props.card)
})
    return(
        <View style={styles.menuBarContainer}>
           <SafeAreaView style={{flex: 0.7}}>
                <FlatList
                    showsHorizontalScrollIndicator={false}


                    horizontal={true}

                    data={props.menu}
                    ItemSeparatorComponent={() => <View style={{width: 5}}/>}
                    persistentScrollbar={true}
                    renderItem={({item, index}) => {

                        return (
                            <View>
                                <TouchableOpacity  onPress={()=>{
                                    props.changeIndex(index)
                                    props.navigation.navigate('Home',{item,index})
                                }}
                                                   style={{

                                    display: 'flex',
                                    flexDirection: 'row',
                                    backgroundColor: props.indexed===index?'black':'white',
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}>
                                    <Text style={{
                                        color: props.indexed===index?'white':'black',
                                        fontSize: 14,
                                        paddingVertical: 20,
                                        paddingHorizontal: 5,
                                        fontFamily: 'Montserrat-Bold',

                                    }}>{item.name}</Text>
                                </TouchableOpacity>
                            </View>
                        );
                    }}
                    keyExtractor={(item, index) => index.toString()}

                />
            </SafeAreaView>
            <View style={styles. basketStyle}>

                <Text
                    style={styles.textBasketStyle}

                >{props.card?.cardData?.length}</Text>
                <Text
                    style={styles.textTotal}
                >{props.tot?.toFixed(3)} DT</Text>

            </View>
        </View>


    )
}
export default MenuBar
