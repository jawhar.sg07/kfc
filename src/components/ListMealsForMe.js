import React, {useEffect, useState} from 'react';
import {
    Dimensions,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    Image,
    ScrollView,
} from 'react-native';
import styles from '../assets/styles';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';



const ListMeals=(props)=>{
    console.log('propssss nextttttttt',props);




    return(
        <View style={styles.MainContainerMeals}>
            <FlatList
                data={props.first}
                renderItem={({ item,index }) => (
                    <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
                        <TouchableOpacity onPress={()=>{
                            props.goToSingle(props.first,props.navigation,index,item)
                        }}   style={styles.ListMealsButton}>
                            <Image  source = {item.image}  style={{width: wp('30%'), height: hp('15%')}}/>

                            <Text
                                style={{
                                    color: '#E82322',
                                    fontSize: 13,
                                    paddingHorizontal: 15,


                                    fontFamily: 'Montserrat-Black',
                                }}>{item.name}</Text>


                        </TouchableOpacity>
                    </View>
                )}
                //Setting the number of column
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>

    )

}
export default ListMeals
