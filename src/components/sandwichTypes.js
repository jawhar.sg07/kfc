import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList, Animated} from 'react-native';

import styles from '../assets/styles';

import Down from '../assets/images/down.svg';
import Up from '../assets/images/up.svg';
import {get, set, round, find} from 'lodash';
import Radio from '../assets/images/radioCheck.svg';
import Circle from '../assets/images/radio.svg';


const SandwichTypes=(props)=>{
    console.log('sanndwicg type',props);
    const [sandwichState,setSandwich]=useState();
    const [selected,setSelected]=useState()
    const showSandwich=()=>{
        setSandwich(true)
    }
    const hideSandwich=()=>{
        setSandwich(false)

    }
    const SelectType=(index,item)=>{
        setSelected(index)
        props.sandwichType(item)
    }
    useEffect(()=>{
        setSandwich(true)
    },[])
    return(
        <View>
            <TouchableOpacity  onPress={()=>{
                {sandwichState ?
                    hideSandwich()
                    :
                    showSandwich()
                }
            }} style={styles.btnSand}>
                <View style={styles.BtnContainerSandwich}>
                    <Text  style={{
                        color: '#000000',
                        fontSize: 15,
                        fontFamily: 'Montserrat-Bold',
                    }}>{props.sandwich.label}</Text>
                    <View style={{display: 'flex', alignItems: 'center', padding: 10}}>
                        <TouchableOpacity style={styles.btnTitleSup}>

                            <Down/>

                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
            {sandwichState &&
            <Animated.View style={styles.animation}>

                <FlatList
                    data={props.sandwich.sub}
                    renderItem={({item,index}) => {
                        return (
                            <TouchableOpacity onPress={()=>SelectType(index,item)} style={styles.btnSandwich}>
                                <View style={styles.btnSubSandwich}>
                                    <Text
                                        style={{
                                            color: '#000000',
                                            fontSize: 14,
                                            marginHorizontal: 10,
                                            fontFamily: 'Montserrat-Bold',
                                        }}>{item.label}</Text>
                                    {selected === index ?
                                        <Radio/>
                                        :
                                        <Circle/>
                                    }
                                </View>
                            </TouchableOpacity>
                        );
                    }}
                    keyExtractor={item => item.label.toString()}
                />

            </Animated.View>
            }
        </View>

    )
}

export default SandwichTypes
