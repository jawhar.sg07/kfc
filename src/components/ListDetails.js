//list for 3rd screen (down and up)
import React, {useEffect, useState} from 'react';
import {
    Dimensions,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    Image,
    ScrollView, Animated,
} from 'react-native';
import styles from '../assets/styles';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Down from '../assets/images/downBlack.svg';
import Up from '../assets/images/upBlack.svg';

const ListDetails = (props) => {
    console.log('prospspspsps form me',props);

    const [Array, setArray] = useState([true]);

    const changeView = (index) => {
        let newArr = [...Array]
        newArr[index]=true
        setArray(newArr)



    };
    const lessView = (index) => {
        let newArr = [...Array]
        newArr[index]=false
        setArray(newArr)



    };
    useEffect(() => {
        for (let i = 1; i < props.single.length; i++) {
            Array[i] = false;


        }

        console.log('ar', Array);
    }, []);

    const Item = (props) => {
        const {item, data, index, navigation} = props;
        console.log('prospeoeopsps', props);

        return (
            <View>
                <TouchableOpacity onPress={() => {
                    {!Array[index] ?

                    changeView(index)
                    :
                        lessView(index)
                    }
                }} style={styles.containerSingle}>
                    <View style={styles.firstList}>
                        <View style={{display: 'flex', flexDirection: 'row'}}>
                            <Text style={{
                                color: 'black',
                                fontSize: 16,
                                fontFamily: 'Montserrat-Black',
                            }}>{item.name}
                            </Text>
                        </View>
                        <View style={{display: 'flex', alignItems: 'center', padding: 10}}>
                            <TouchableOpacity>
                                {Array[index]&& data!==undefined ?
                                    <Up/>
                                    :
                                    <Down/>
                                }
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={{borderWidth: 0.6, borderColor: '#707070', opacity: 0.3, marginHorizontal: 15}}></View>
                {Array[index]==true &&
                <Animated.View style={styles.animationSingle}>
                    <FlatList
                        data={data}
                        renderItem={({item}) => {
                            return (

                                <TouchableOpacity onPress={() => {
                                    navigation.navigate('OrderDetails', {item});
                                    console.log('here navvab', props.navigation);
                                }} style={styles.btnListDetails}>
                                    <View style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        flex: 0.6,
                                        marginHorizontal: 10,
                                        marginVertical: 15,
                                    }}>
                                        <Text
                                            style={{
                                                color: '#E82322',
                                                fontSize: 14,
                                                paddingHorizontal: 15,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>{item.name}</Text>
                                        <Text
                                            style={{
                                                color: '#E82322',
                                                fontSize: 14,
                                                paddingHorizontal: 15,
                                                fontFamily: 'Montserrat-Bold',
                                                marginBottom: 5,
                                            }}>{item.fam}</Text>
                                        <Text
                                            style={{
                                                color: 'black',
                                                fontSize: 13,
                                                paddingHorizontal: 15,
                                                fontFamily: 'Montserrat-Bold',
                                                marginVertical: 5,
                                            }}>{item.price} DT</Text>
                                        <Text
                                            style={{
                                                color: '#474545',
                                                fontSize: 12,
                                                paddingHorizontal: 15,
                                                fontFamily: 'Montserrat-Regular',
                                            }}>{item.description}</Text>
                                    </View>
                                    <View style={{display: 'flex', flex: 0.4, justifyContent: 'center'}}>
                                        <Image source={item.img} style={{width: wp('35%'), height: hp('25%')}}/>
                                    </View>
                                </TouchableOpacity>
                            );
                        }}
                        keyExtractor={item => item.name.toString()}
                    />
                </Animated.View>
                }

            </View>
        );
    };
    return (
        <FlatList
            data={props.first}
            renderItem={({item, index}) => (
                <Item
                    item={item}
                    data={item.meals}
                    index={index}
                    navigation={props.navigation}
                />
            )}
            //Setting the number of column
            keyExtractor={(item, index) => index.toString()}
        />
    );
};
export default ListDetails;
