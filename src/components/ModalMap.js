import React, {useEffect, useState,Component} from 'react';
import {
    Dimensions,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    Image,
    ScrollView,
    TouchableHighlight,
    PermissionsAndroid,
    Modal,
} from 'react-native';
import geolocation from '@react-native-community/geolocation';


import Geography from '../utlis/geography'

const { width, height } = Dimensions.get('window')
import Back from '../assets/images/back.svg';
import MapView, {Marker, PROVIDER_GOOGLE, Polygon, Callout} from 'react-native-maps';
const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = 0.04796531791907514

class ModalMap extends Component{
    constructor(props) {
        super(props);
        this._map = null;
        this.mapLoaded = 0;
        this.state = {
            region: {
                latitude: 35.05527751055514,
                longitude:9.525643195956945,
                latitudeDelta: 5.051808148647687,
                longitudeDelta: 3.4627643600106213
            },

            showReloadButton: false,
            animationIn: null,
            animationOut: null,
            isModalVisible: false
        };
    }
    componentDidMount() {
        console.log('prororororor',this.props);

    }
    render(){
        console.log('initial state',this.state.region);
        return(
            <Modal
                animationType="slide"

                visible={this.props.modalMap}


            >

                <View style={{height: "100%", width: "100%", flex: 1}}>
                    <View style={{padding:20,backgroundColor:'white'}}>
                        <TouchableOpacity onPress={() => {
                            this.props.closeModalMap();
                        }}>
                            <Back/>
                        </TouchableOpacity>

                    </View>
                    <MapView
                        initialRegion={this.state.region} provider={PROVIDER_GOOGLE} ref={ref => this._map = ref}
                        customMapStyle={Geography.getStyle()} style={{height: "100%", width: "100%", flex: 1}}
                        showsCompass={true} showsScale={true} toolbarEnabled={true} cacheEnabled={true}
                        showsUserLocation={true} followsUserLocation={true} showsMyLocationButton={true}
                        loadingEnabled={true}
                    >
                        {this.props.place.map(feature => (

                            <Marker
                               coordinate={{longitude:Number(feature.latitude),latitude:Number(feature.longitude)}}
                                title={feature.label} stopPropagation={true}
                               description={feature.description}
                               onPress={(ddd)=>{
                                   this.props.savePoint(feature)
                                   this.props.openModalRecap();
                               }}


                            >

                            </Marker>
                            ))}
                    </MapView>
                </View>
            </Modal>

        )
    }

}
export default ModalMap
