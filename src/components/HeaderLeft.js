import React, { useEffect,useState } from 'react';
import { Dimensions, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Menu from '../assets/images/menu.svg'
import Kfc from '../assets/images/kfc.svg'

const HeaderLeft=(props)=> {

    const onLeftControlPress=({navigation})=>{
       props.navigation.openDrawer();

    }

    return (
        <View style={{display:'flex',flexDirection:'row',marginHorizontal:15}}>
            <TouchableOpacity onPress={onLeftControlPress}>
       <Menu/>
            </TouchableOpacity>
       <Kfc/>
    </View>
    )
}
export default HeaderLeft
