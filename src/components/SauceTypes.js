import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList, Animated} from 'react-native';
import styles from '../assets/styles';
import Down from '../assets/images/down.svg';
import Radio from '../assets/images/radioCheck.svg';
import Circle from '../assets/images/radio.svg';
import Checked from '../assets/images/check.svg'
import Square from'../assets/images/square.svg'

const SauceTypes=(props)=>{
    const [sandwichState,setSandwich]=useState(false);
    const [selected,setSelected]=useState(null)
    const [Array, setArray] = useState([]);
    const showSandwich=()=>{
        setSandwich(true)
    }
    const hideSandwich=()=>{
        setSandwich(false)
        setSelected(null)
    }
    const SelectType=(index,item)=>{
        let newArr = [...Array]

        newArr[index]=true

        setArray(newArr)
        props.sauceType(item)
    }
    const UncheckedItem=(index,item)=>{
        let newArr = [...Array]

        newArr[index]=false

        setArray(newArr)
        props.SliceSauce(item)


    }
    useEffect(() => {
        for (let i = 0; i < props.sauce.sub?.length; i++) {
            Array[i] = false;

        }
        console.log('ar', Array);
    }, []);

    return(
        <View>
            <TouchableOpacity  onPress={()=>{
                {sandwichState ?
                    hideSandwich()
                    :
                    showSandwich()
                }
            }} style={styles.btnSand}>
                <View style={styles.BtnContainerSandwich}>
                    <Text  style={{
                        color: '#000000',
                        fontSize: 15,
                        fontFamily: 'Montserrat-Bold',
                    }}>{props.sauce.label}</Text>
                    <View style={{display: 'flex', alignItems: 'center', padding: 10}}>
                        <TouchableOpacity style={styles.btnTitleSup}>

                            <Down/>

                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
            {sandwichState &&
            <Animated.View style={styles.animation}>

                <FlatList
                    data={props.sauce.sub}
                    renderItem={({item,index}) => {
                        return (
                            <TouchableOpacity onPress={()=>{{!Array[index] ?

                                SelectType(index,item)
                                :
                                UncheckedItem(index,item)
                            }

                            }} style={styles.btnSandwich}>
                                <View style={styles.btnSubSandwich}>
                                    <View style={{display:'flex',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                                        <Image style={{width:35,height:50}} source={item.image}/>
                                        <View style={{display:'flex',flexDirection:'column',justifyContent:'center'}}>

                                            <Text
                                                style={{
                                                    color: '#000000',
                                                    fontSize: 14,
                                                    marginHorizontal: 10,
                                                    fontFamily: 'Montserrat-Bold',
                                                    marginBottom:5
                                                }}>{item.label}</Text>

                                        </View>
                                    </View>

                                    {Array[index] ?
                                        <View style={{backgroundColor:'#E3002B',paddingHorizontal:5,paddingVertical:8}}>
                                        <Checked/>
                                        </View>
                                        :

                                        <Square/>

                                    }
                                </View>
                            </TouchableOpacity>
                        );
                    }}
                    keyExtractor={item => item.label.toString()}
                />



            </Animated.View>

            }
        </View>

    )
}

export default SauceTypes
