import React, {useEffect, useState} from 'react';
import {Animated, FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import styles from '../assets/styles';
import Down from '../assets/images/downr.svg';
import Up from '../assets/images/upBlack.svg';

const ServiceType = () => {
    const matiere = [
        {
            name: 'Matiére',
            details: [
                {
                    name: 'Information Commande',
                }, {
                    name: 'Réclamation',
                }, {
                    name: 'Service Client',
                },
            ],
        },
    ];
    const [service, setService] = useState(false);
    const [selected, setSelected] = useState('');
    const changeView = (index) => {
        setService(true);
    };
    const lessView = (index) => {
        setService(false);


    };
    const Item = (props) => {
        const {item, data, index} = props;
        return (
            <View style={{
                backgroundColor: 'white', shadowColor: '#000',
                shadowRadius: 2, elevation: !service ? 0 : 5, marginHorizontal: 10, marginVertical: 5,
            }}>
                <TouchableOpacity onPress={() => {
                    {
                        !service ?

                            changeView(index)
                            :
                            lessView(index);
                    }
                }}
                                  style={styles.containerContact(service)}>
                    <View style={styles.firstList}>
                        <View style={{display: 'flex', flexDirection: 'column'}}>
                            <Text style={{
                                color: 'black',
                                fontSize: 14,
                                fontFamily: 'Montserrat-Bold',
                            }}>{item.name}
                            </Text>
                            <Text style={{
                                color: '#717070',
                                fontSize: 12,
                                fontFamily: 'Montserrat-Medium',
                            }}>{selected}
                            </Text>
                        </View>
                        <View style={{display: 'flex', alignItems: 'center', padding: 10}}>
                            <TouchableOpacity>

                                {service && data !== undefined ?
                                    <Up fill='#000'/>
                                    :
                                    <Down/>
                                }

                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableOpacity>

                {service &&
                <Animated.View style={styles.animationSingle}>
                    <FlatList
                        data={data}
                        renderItem={({item}) => {
                            return (
                                <TouchableOpacity onPress={() => {
                                    setSelected(item.name);
                                    setService(false)
                                }} style={styles.containerSubContact}>
                                    <View style={{marginVertical: 10}}>
                                        <Text
                                            style={{
                                                color: 'black',
                                                fontSize: 14,
                                                paddingHorizontal: 15,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>{item.name}</Text>
                                    </View>

                                </TouchableOpacity>

                            );
                        }}
                        keyExtractor={item => item.name.toString()}
                    />
                </Animated.View>
                }
            </View>
        );
    };
    return (
        <FlatList
            data={matiere}
            renderItem={({item, index}) => (
                <Item
                    item={item}
                    data={item.details}
                    index={index}

                />
            )}
            //Setting the number of column
            keyExtractor={(item, index) => index.toString()}
        />
    );
};
export default ServiceType;
