import React, {useEffect, useState} from 'react';
import {

    Text,
    TouchableOpacity,
    View,

    Modal,
} from 'react-native';

import styles from '../assets/styles';
import PaymentLists from './PaymentLists';

const ModalEat = (props) => {

    return (

        <Modal
            animationType="slide"

            visible={props.eat}


        >
            <View style={styles.centeredView}>


                    <View style={{display:'flex',flex:0.4,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{
                            color: '#000',
                            fontSize: 17,
                            fontFamily: 'Montserrat-Black',
                        }}>AWeSOME ! DO YOU WANT ?</Text>
                        <Text style={{
                            color: '#000',
                            fontSize: 17,
                            fontFamily: 'Montserrat-Black',
                            marginBottom:15
                        }}>TO EAT IN OR TAKE OUT ?</Text>
                        <Text style={{
                            color: '#474545',
                            fontSize: 13,
                            fontFamily: 'Montserrat-Regular',
                        }}>Choose how you'd like to collect</Text>
                        <Text style={{
                            color: '#474545',
                            fontSize: 13,
                            fontFamily: 'Montserrat-Regular',
                        }}> your order</Text>


                    </View>

                    <View style={{display:'flex',flex:0.6}}>
                        <View style={{marginBottom:20}}>
                        <TouchableOpacity onPress={()=>{
                            props.closeEat();
                            props.closeCollection()
                        }} style={{
                            alignItems: 'center',
                            backgroundColor: '#E3002B',
                            borderRadius: 50,
                            padding: 16,
                            marginVertical: 10,
                            marginHorizontal: 10,
                        }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >EAT IN</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{
                            props.closeEat();
                        }} style={{
                            alignItems: 'center',
                            backgroundColor: '#E3002B',
                            borderRadius: 50,
                            padding: 16,
                            marginVertical: 10,
                            marginHorizontal: 10,
                        }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >TAKE OUT</Text>
                        </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => {
                            props.closeEat();

                        }} style={{
                            alignItems: 'center',
                            backgroundColor: 'white',
                            borderRadius: 50,
                            padding: 16,
                            borderColor:'black',
                            borderWidth:1,
                            marginHorizontal: 10,
                        }}

                        >
                            <Text
                                style={{
                                    color: 'black',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',

                                }}

                            >CANCEL</Text>
                        </TouchableOpacity>
                    </View>




            </View>
        </Modal>


    );

};

export default ModalEat
