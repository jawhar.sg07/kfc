import React, {useEffect, useState} from 'react';
import {
    Text,
    TouchableOpacity,
    View,
    FlatList,
    Modal,
    TextInput, ScrollView,
} from 'react-native';
import styles from '../assets/styles';
import Back from '../assets/images/back.svg';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Next from '../assets/images/next.svg';

const ModalReacapChoice = (props) => {
    console.log('reacapppppppppp',props);

    return (
        <Modal
            animationType="slide"

            visible={props.modalRecap}


        >
            <ScrollView>
            <View style={styles.centeredView}>
                <View style={{flex: 0.7}}>
                    <View style={styles.modalView}>
                        <TouchableOpacity onPress={() => {
                            props.closeModalRecap();
                            props.openModalOrder();
                        }}>
                            <Back/>
                        </TouchableOpacity>

                    </View>
                    <View style={{display: 'flex', alignItems: 'center', flexDirection: 'column'}}>
                        <Text style={{
                            color: 'black',
                            fontSize: 16,
                            fontFamily: 'Montserrat-SemiBold',
                            marginBottom: 15,
                        }}>{props.point?.label}</Text>
                        <TouchableOpacity onPress={() => {
                            props.closeModalRecap();

                            props.openModalOrder();
                        }} style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: 'white',
                            marginBottom: 20,


                        }}

                        >
                            <Text
                                style={{
                                    color: '#E3002B',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-ExtraBold',
                                    marginHorizontal: 15,
                                }}

                            >change</Text>
                            <Next/>
                        </TouchableOpacity>
                        <Text style={{
                            color: 'black',
                            fontSize: 16,
                            fontFamily: 'Montserrat-Black',
                            marginBottom: 5,
                        }}>TELL US HOW YOU'D LIKE IT</Text>
                    </View>
                    <View style={{display: 'flex', flexDirection: 'column', marginHorizontal: 15, marginVertical: 20}}>
                        <TouchableOpacity style={styles.btnContainerRecap}>

                            <View style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}>
                                <View>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 12,
                                        fontFamily: 'Montserrat-Regular',
                                        marginBottom: 10,
                                    }}>Date :</Text>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 14,
                                        fontFamily: 'Montserrat-Bold',
                                    }}>TODAY</Text>
                                </View>
                                <TouchableOpacity style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: 'white',
                                }}

                                >
                                    <Text
                                        style={{
                                            color: 'black',
                                            fontSize: 15,
                                            fontFamily: 'Montserrat-ExtraBold',
                                            marginHorizontal: 15,
                                        }}

                                    >change</Text>
                                    <Next/>
                                </TouchableOpacity>
                            </View>


                            <View style={{
                                display: 'flex',
                                flex: 0.8,
                                justifyContent: 'flex-end',
                                flexDirection: 'row',
                            }}>

                            </View>


                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnContainerRecap}>

                            <View style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}>
                                <View>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 12,
                                        fontFamily: 'Montserrat-Regular',
                                        marginBottom: 10,
                                    }}>Time :</Text>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 14,
                                        fontFamily: 'Montserrat-Bold',
                                    }}>7 PM</Text>
                                </View>
                                <TouchableOpacity style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: 'white',
                                }}

                                >
                                    <Text
                                        style={{
                                            color: 'black',
                                            fontSize: 15,
                                            fontFamily: 'Montserrat-ExtraBold',
                                            marginHorizontal: 15,
                                        }}

                                    >change</Text>
                                    <Next/>
                                </TouchableOpacity>
                            </View>


                            <View style={{
                                display: 'flex',
                                flex: 0.8,
                                justifyContent: 'flex-end',
                                flexDirection: 'row',
                            }}>

                            </View>


                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{display: 'flex', flex: 0.3, flexDirection: 'column'}}>
                    <View>
                        <TouchableOpacity onPress={() => {
                            props.closeModalRecap();
                            props.closeModalVisible();
                            props.closeModalOrder();
                            props.closeModalMap()
                        }} style={{
                            alignItems: 'center',
                            backgroundColor: '#E3002B',
                            borderRadius: 50,
                            padding: 16,
                            marginVertical: 20,
                            marginHorizontal: 10,
                        }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >START MY ORDER</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {

                            props.resetOrder();
                            props.closeModalRecap();
                            props.closeModalVisible();
                            props.closeModalOrder();
                            props.closeModalMap();

                        }} style={{
                            alignItems: 'center',
                            backgroundColor: 'white',
                            borderRadius: 50,
                            padding: 16,
                            borderWidth: 1,
                            borderColor: 'black',

                            marginHorizontal: 10,
                            marginVertical:20
                        }}

                        >
                            <Text
                                style={{
                                    color: 'black',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >CANCEL</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{display: 'flex', justifyContent: 'flex-end', flex: 1, alignItems: 'center'}}>
                        <View style={styles.threeTextContainer}>
                            <View style={styles.boxSimple}>
                            </View>
                            <View style={styles.boxSimple}>
                            </View>
                            <View style={styles.boxSimple}>
                            </View>

                        </View>
                    </View>
                </View>
            </View>
            </ScrollView>
        </Modal>
    );
};

export default ModalReacapChoice;


