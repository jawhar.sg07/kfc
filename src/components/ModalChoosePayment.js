import React, {useEffect, useState} from 'react';
import {

    Text,
    TouchableOpacity,
    View,

    Modal,
} from 'react-native';

import styles from '../assets/styles';
import PaymentLists from './PaymentLists';

const ModalChoosePayment = (props) => {
    const [value, setValue] = React.useState('first');
    useEffect(()=>{
        props.getPaymentMethods();
    },[])
    return (

        <Modal
            animationType="slide"

            visible={props.payment}


        >
            <View style={styles.centeredView}>
                <View>
                    <View style={styles.modalView}>
                        <Text style={{
                            color: '#000',
                            fontSize: 16,
                            fontFamily: 'Montserrat-Black',
                        }}>SELECT PAYMENT</Text>

                    </View>


                </View>
                <View>
                    <PaymentLists {...props}/>

                </View>
            </View>
        </Modal>


    );

};

export default ModalChoosePayment;
