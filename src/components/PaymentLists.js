import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList, Animated} from 'react-native';
import styles from '../assets/styles';
import Radio from '../assets/images/radioCheck.svg';
import Circle from '../assets/images/radio.svg';

const PaymentLists = (props) => {
    console.log('props choose',props);
    let [selected, setSelected] = React.useState([0]);
    let [second, setSecond] = React.useState([0]);
    const [current, setCurrent] = useState([]);
    const [way,setWay]=useState(props.payments[0])
    const changeRadio = (index,item) => {
        setSelected(index);
        setWay(item)
        setSecond(0)
    };
    useEffect(()=>{
        setSelected(0)
        setSecond(0)
    setCurrent([])
    },[])
const changeSecond=(index)=>{
    setSecond(index)
}
    const Item = (props) => {
        const {item, index, data} = props;
        console.log('list item payment', props);

        return (
            <View>
                <TouchableOpacity onPress={() => {
                    changeRadio(index,item);
                    setCurrent(data)
                }} style={styles.btnListDetails}>
                    <View style={{
                        display: 'flex',
                        flexDirection: 'row',
                        padding: 15,
                        justifyContent: 'space-between',
                        flex: 1,
                        alignItems: 'center',
                        backgroundColor: '#F5F5F5',

                    }}>
                        <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                            <View style={{marginRight: 10}}>


                            </View>
                            <View style={{display: 'flex', flexDirection: 'row'}}>
                                {selected === index ?
                                    <Radio/>
                                    :
                                    <Circle/>
                                }
                                <Text
                                    style={{
                                        color: '#000000',
                                        fontSize: 15,

                                        fontFamily: 'Montserrat-Bold',
                                        marginHorizontal: 10,

                                    }}>{item.label}</Text>
                            </View>
                        </View>
                        <View style={{display: 'flex', alignItems: 'center', padding: 10}}>

                        </View>

                    </View>

                </TouchableOpacity>
                <Animated.View style={styles.paymentAnimation}>
                    {current.length !==0 &&
                    <FlatList
                        data={data}
                        renderItem={({item,index}) => {
                            return (

                                <TouchableOpacity onPress={()=>changeSecond(index)} style={{

                                    borderWidth: 1,
                                    borderRadius: 5,
                                    borderColor:'#ECEAEA',
                                    marginVertical:5
                                }}>
                                    <View style={{
                                        display: 'flex',
                                        flexDirection: 'row',
                                        padding: 10,
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent:'space-between'


                                    }}>


                                        <View style={{display: 'flex', flexDirection: 'row',marginHorizontal:10}}>
                                            {second === index ?
                                                <Radio/>
                                                : <Circle/>
                                            }
                                            <Text
                                                style={{
                                                    color: '#000000',
                                                    fontSize: 14,
                                                    marginHorizontal: 10,
                                                    fontFamily: 'Montserrat-Bold',

                                                }}>{item.label}</Text>

                                        </View>
                                       <Image source={item.image}/>
                                    </View>


                                </TouchableOpacity>


                            );
                        }}
                        keyExtractor={item => item.label.toString()}
                    />}
                </Animated.View>


            </View>
        );
    };
    return (
        <View>
        <FlatList

            data={props.payments}
            renderItem={({item, index}) => (
                <Item
                    item={item}
                    index={index}
                    data={item.sub}


                />


            )}
            //Setting the number of column

            keyExtractor={(item, index) => index.toString()}
        />
            <View>
                <TouchableOpacity onPress={()=>{
                    props.saveWay(way,second)
                    props.closePayment();
                }} style={{
                    alignItems: 'center',
                    backgroundColor: '#E3002B',
                    borderRadius: 50,
                    padding: 16,
                    marginVertical: 20,
                    marginHorizontal: 10,
                }}

                >
                    <Text
                        style={{
                            color: 'white',
                            fontSize: 15,
                            fontFamily: 'Montserrat-Bold',
                        }}

                    >SAVE DETAILS</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    props.closePayment();

                }} style={{
                    alignItems: 'center',
                    backgroundColor: 'white',
                    borderRadius: 50,
                    padding: 16,
                    borderColor:'black',
                    borderWidth:1,
                    marginHorizontal: 10,
                }}

                >
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 15,
                            fontFamily: 'Montserrat-Bold',

                        }}

                    >CANCEL</Text>
                </TouchableOpacity>
            </View>

        </View>

    );
};
export default PaymentLists;
