import React,{useEffect,useState} from 'react';
import {View, Button, TextInput, StyleSheet, Text, TouchableOpacity} from 'react-native';
;
import { CheckBox } from 'react-native-elements'
const Form = (props) => {
    console.log('props form',props);
    const [sms, setSms] = useState(false)
    const [email, setEmail] = useState(false)
    const [condition, setCondition] = useState(false)
   const payload={};
    return (
        <View style={styles.root}>
            <View style={{marginHorizontal:8}}>
                <TextInput
                    placeholder={'First name'}
                    style={styles.input}
                    placeholderTextColor={'black'}
                    onChangeText={(text)=>payload.firstName=text}
                />
                <TextInput

                    placeholder={'Last name'}
                    style={styles.input}
                    placeholderTextColor={'black'}
                    onChangeText={(text)=>payload.lastName=text}
                />
                <TextInput
                    placeholder={'Email address'}

                    style={styles.input}
                    placeholderTextColor={'black'}
                    onChangeText={(text)=>payload.email=text}
                />
                <TextInput
                    placeholder={'Phone number'}
                    keyboardType={'decimal-pad'}
                    style={styles.input}
                    placeholderTextColor={'black'}
                    onChangeText={(text)=>payload.phoneNumber=text}
                />
            </View>
            <View>
                <CheckBox
                    size={35}
                    checked={sms}
                    onPress={()=>setSms(!sms)}
                    checkedColor={'#000000'}
                    title='Contact me about my order via sms'
                />
                <CheckBox
                    size={35}
                    checked={email}
                    onPress={()=>setEmail(!email)}
                    checkedColor={'#000000'}
                    title='Email me awesome offers'
                />
                <CheckBox
                    size={35}
                    checked={condition}
                    onPress={()=>setCondition(!condition)}
                    checkedColor={'#000000'}
                    title='Accept terms and conditions and privacy statement'
                />
            </View>
            <TouchableOpacity onPress={()=>{
                props.saveProfileInformation(payload)
                props.closeUser();
                console.log('payl',payload);
            }} style={{
                alignItems: 'center',
                backgroundColor: '#E3002B',
                borderRadius: 50,
                padding: 16,
                marginVertical: 20,
                marginHorizontal: 10,
            }}

            >
                <Text
                    style={{
                        color: 'white',
                        fontSize: 15,
                        fontFamily: 'Montserrat-Bold',
                    }}

                >SAVE DETAILS</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
                props.closeUser();

            }} style={{
                alignItems: 'center',
                backgroundColor: 'white',
                borderRadius: 50,
                padding: 16,
                borderColor: 'black',
                borderWidth: 1,
                marginHorizontal: 10,
            }}

            >
                <Text
                    style={{
                        color: 'black',
                        fontSize: 15,
                        fontFamily: 'Montserrat-Bold',

                    }}

                >CANCEL</Text>
            </TouchableOpacity>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    checkboxContainer: {
        flexDirection: "row",
    },
    checkbox: {
        alignSelf: "center",
    },
    label: {
        margin: 8,
    },
    root: {
        justifyContent: 'center'
    },
    input: {
        padding: 10,
        marginBottom: 8,
        borderColor: '#F5F5F5',
        borderWidth: 1,
        backgroundColor:'#F5F5F5',
    },
    inputContact: {
        backgroundColor:'#FFF',
        padding: 'offset'
    }
});
export default Form;
