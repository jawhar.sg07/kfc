import React, { useEffect,useState } from 'react';
import {Dimensions, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, Image, Alert} from 'react-native';
import { connect } from 'react-redux';
import Menu from '../assets/images/menu.svg'
import Kfc from '../assets/images/kfc.svg'
import Login from '../assets/images/login.svg'

const HeaderRight=(props)=> {
    return (
        <View>
        <View style={{display:'flex',flexDirection:'row',marginHorizontal:10,justifyContent:'space-around',alignItems:'center'}}>
            <Image source={require('../assets/images/reward1.png')} style={{width:18,height:30}} />
            <Text style={{color:'#000000',fontSize:14,fontFamily:'Montserrat-Black',marginLeft:5,marginRight:15}}>Rewards</Text>
            <View style={{marginHorizontal:10}}>
                <TouchableOpacity onPress={()=>Alert.alert('NOT YET !!')}>
            <Login/>
                </TouchableOpacity>
            </View>
        </View>
        </View>
    )
}
export default HeaderRight
