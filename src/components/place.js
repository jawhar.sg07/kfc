import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView} from 'react-native';
import Next from '../assets/images/next.svg';


const Place=(props)=>{
    return(
        <View style={{
            backgroundColor: 'white',

            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom:10
        }}>
            <View style={{
                display: 'flex',
                flexDirection: 'column',
                flex: 0.95,
                justifyContent: 'center',

            }}>
                <Text style={{
                    color: '#474545',
                    fontSize: 12,
marginBottom:5,
                    fontFamily: 'Montserrat-Bold',
                }}>Livraison restaurant:</Text>
                <Text style={{
                    color: '#000000',
                    fontSize: 14,
                    fontFamily: 'Montserrat-Bold',
                }}>{props.point?.label}</Text>
            </View>
            <View>
                <TouchableOpacity onPress={() => {
                   props. openModalChoose();
                }} style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',

                    backgroundColor: 'white',
                }}

                >
                    <Text
                        style={{
                            color: '#E3002B',
                            fontSize: 15,
                            fontFamily: 'Montserrat-ExtraBold',
                            marginRight:5

                        }}

                    >change</Text>
                    <Next/>
                </TouchableOpacity>

            </View>

        </View>
    )
}
export default Place
