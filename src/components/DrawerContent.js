import React, {useEffect, useState} from 'react';
import {

    Text,
    TouchableOpacity,
    View,
    Image,

    Modal,
} from 'react-native';

import styles from '../assets/styles';
import Close from '../assets/images/close.svg';

const DrawerContent = (props) => {
    console.log('container props', props);
    return (
        <View style={{display: 'flex', flexDirection: 'column', flex: 1}}>
            <View style={styles.closeDrawerView}>
                <TouchableOpacity onPress={() => {
                    props.navigation.closeDrawer();
                }}>
                    <Close/>
                </TouchableOpacity>

            </View>
            <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>


                <Image style={{width: 200, height: 70}} resizeMode={'contain'}
                       source={require('../assets/images/drawerImage.png')}/>
            </View>
            <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', marginVertical: 25}}>
                <View style={styles.btnDrawer}>
                    <Image style={{width: 40, height: 20}} source={require('../assets/images/en.png')}/>
                    <Text style={styles.textDrawer}>English</Text>
                </View>
                <View style={styles.btnDrawer}>
                    <Image style={{width: 40, height: 20}} source={require('../assets/images/france.png')}/>
                    <Text style={styles.textDrawer}>Frensh</Text>
                </View>
            </View>
            <View style={{borderWidth: 0.6, borderColor: '#707070', opacity: 0.3, marginHorizontal: 10}}></View>
            <View style={{marginHorizontal: 10, marginVertical: 10}}>


                <TouchableOpacity onPress={() => {
                    props.navigation.navigate('AccountScreen');
                }} style={styles.btnDrawerItem}
                >

                    <Text style={{color: '#000000', fontFamily: 'Montserrat-Bold', fontSize: 15}}>MON COMPTE</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnDrawerItem} onPress={() => {
                    props.navigation.navigate('AllTickets');
                }}>

                    <Text style={{color: '#000000', fontFamily: 'Montserrat-Bold', fontSize: 15}}>HISTORIQUE DES
                        COMMANDES</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnDrawerItem} onPress={() => {

                }}>

                    <Text style={{color: '#000000', fontFamily: 'Montserrat-Bold', fontSize: 15}}>MON PANIER</Text>
                </TouchableOpacity>

                <View style={{
                    borderWidth: 0.6,
                    borderColor: '#707070',
                    opacity: 0.3,
                    marginHorizontal: 10,
                    marginVertical: 15,
                }}></View>
                <TouchableOpacity style={styles.btnDrawerItem} onPress={() => {
                    props.navigation.navigate('OurMenu');
                }}>

                    <Text style={{color: '#000000', fontFamily: 'Montserrat-Bold', fontSize: 15}}>NOTRE CARTE</Text>
                </TouchableOpacity>


                <TouchableOpacity style={styles.btnDrawerItem} onPress={() => {
                    props.navigation.navigate('OurAdventure');

                }}>

                    <Text style={{color: '#000000', fontFamily: 'Montserrat-Bold', fontSize: 15}}>NOTRE AVENTURE</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.btnDrawerItem} onPress={() => {
                    props.navigation.navigate('Contact');
                }}>

                    <Text style={{color: '#000000', fontFamily: 'Montserrat-Bold', fontSize: 15}}>NOUS CONTACTER</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnDrawerItem} onPress={() => {
                    props.navigation.navigate('FAQ');
                }}>

                    <Text style={{color: '#000000', fontFamily: 'Montserrat-Bold', fontSize: 15}}>FAQ</Text>
                </TouchableOpacity>


            </View>
            <View style={{display: 'flex', justifyContent: 'flex-end', flex: 1, alignItems: 'center'}}>
                <View style={styles.threeTextContainer}>
                    <View style={styles.boxSimple}>
                    </View>
                    <View style={styles.boxSimple}>
                    </View>
                    <View style={styles.boxSimple}>
                    </View>

                </View>
            </View>
        </View>
    );
};


export default DrawerContent;
