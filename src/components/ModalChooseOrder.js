import React, {useEffect, useState} from 'react';
import {
    Dimensions,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    Image,
    ScrollView,
    TouchableHighlight,
    Modal,
} from 'react-native';
import styles from '../assets/styles';
import Back from '../assets/images/back.svg';

const ModalChooseOrder = (props) => {

    return (

        <Modal
            animationType="slide"

            visible={props.modalVisible}


        >
            <View style={styles.centeredView}>
                <View style={{flex: 0.4}}>
                    <View style={styles.modalView}>
                        <TouchableOpacity onPress={() => {
                            props.closeModalVisible();
                        }}>
                            <Back/>
                        </TouchableOpacity>

                    </View>
                    <View style={{display: 'flex', alignItems: 'center', flexDirection: 'column'}}>
                        <Text style={{
                            color: 'black',
                            fontSize: 16,
                            fontFamily: 'Montserrat-Black',
                            marginBottom: 5,
                        }}>TELL US HOW YOU'D LIKE IT</Text>
                        <Text style={{
                            color: '#474545',
                            fontSize: 12,
                            fontFamily: 'Montserrat-Regular',
                        }}>Choose your type for delivery</Text>
                    </View>
                </View>
                <View style={{display: 'flex', flex: 0.6, flexDirection: 'column'}}>
                    <View>
                        <TouchableOpacity style={{
                            alignItems: 'center',
                            backgroundColor: '#E3002B',
                            borderRadius: 50,
                            padding: 16,
                            marginVertical: 20,
                            marginHorizontal: 10,
                        }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >ORDER DELIVERY</Text>
                        </TouchableOpacity>
                        <TouchableOpacity   onPress={()=>{
                            props.closeModalVisible();
                            props.openModalOrder();

                        }} style={{
                            alignItems: 'center',
                            backgroundColor: '#E3002B',
                            borderRadius: 50,
                            padding: 16,

                            marginHorizontal: 10,
                        }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >ORDER DELIVERY</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{display:'flex',justifyContent:'flex-end',flex:1,alignItems:'center'}}>
                        <View style={styles.threeTextContainer}>
                            <View style={styles.boxSimple}>
                            </View>
                            <View style={styles.boxSimple}>
                            </View>
                            <View style={styles.boxSimple}>
                            </View>

                        </View>
                    </View>
                </View>
            </View>
        </Modal>


    );

};

export default ModalChooseOrder;
