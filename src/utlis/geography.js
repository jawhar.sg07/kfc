import Geolocation from '@react-native-community/geolocation';
import EventEmitter from 'events';
import merge from "lodash/merge";
import Promise from "bluebird";

const options = {
    enableHighAccuracy: true,
    timeout: 2000,
    maximumAge: 1000
};
const useSignificantChanges = true;

class Geography {
    latitude = 0;
    longitude = 0;

    constructor() {
        new Promise((resolve, reject) => {
            Geolocation.getCurrentPosition(resolve, reject, options)
        }).then(({coords: {latitude, longitude}}) => {
            console.log('latitude',latitude);
            console.log('longi',longitude);
            this.setLatitude(latitude);
            this.setLongitude(longitude);
        }).catch(console.warn);
        Geolocation.watchPosition(({coords: {latitude, longitude}}) => {
            this.setLatitude(latitude);
            this.setLongitude(longitude);
            this._emitter.emit('change');
        }, console.warn, merge(options, {useSignificantChanges}));
        this._emitter = new EventEmitter();
    }

    onChange(fn) {
        this._emitter.addListener('change', fn);
    }

    setLatitude(latitude) {
        this.latitude = latitude;
    }

    setLongitude(longitude) {
        this.longitude = longitude;
    }

    static toRadians(degrees) {
        return degrees * Math.PI / 180;
    }

    distance(latitude, longitude, latitude2 = this.latitude, longitude2 = this.longitude) {
        const radius = 6371000;

        const dLat = Geography.toRadians(latitude2 - latitude);
        const dLon = Geography.toRadians(longitude2 - longitude);

        latitude = Geography.toRadians(latitude);
        latitude2 = Geography.toRadians(latitude2);

        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(latitude) * Math.cos(latitude2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return radius * c;
    }

    getStyle() {
        return [
            {
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "weight": "2.00"
                    }
                ]
            },
            {
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#E3002B"
                    }
                ]
            },
            {
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "stylers": [
                    {
                        "color": "#E3002B"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#E3002B"
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#E3002B"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#E3002B"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "color": "#E3002B"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#c8d7d4"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#7a7a7a"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#E3002B"
                    }
                ]
            }
        ];
    }
}

export default new Geography();
