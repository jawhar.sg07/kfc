import axios from 'axios';
import { ENTRYPOINT } from '../config/entrypoints' ;

const settings = {
    baseURL: ENTRYPOINT,
    timeout: 30000
};

export default axios.create(settings);
