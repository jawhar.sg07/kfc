import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import React from 'react'
import HeaderRight from '../components/HeaderRight';
import OrderDetails from '../scenes/OrderDetails';
import {createAppContainer} from 'react-navigation';
import RecapOrder from '../scenes/RecapOrder';
import Home from '../scenes/Home';
import OurMenu from '../scenes/ourMenu'
import DetailsScreen from '../scenes/DetailsScreen';
import { Dimensions, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import HeaderLeft from '../components/HeaderLeft';
import CheckoutScreen from '../scenes/CheckoutScreen';
import RecapCheckout from '../scenes/RecapCheckout';
import DrawerContent from '../components/DrawerContent';
import AccountScreen from '../scenes/AccountScreen';
import PersonalInfos from '../scenes/personalInfos'
import Login from '../scenes/login';
import OurAdventure from '../scenes/ourAdventure';
import FAQ from '../scenes/FAQ';
import EndCheckout from '../scenes/endCheckout'
import AllTickets from '../scenes/allTickets'
import Contact from '../scenes/contact'
export const width = Math.round(Dimensions.get('window').width);
export const height = Math.round(Dimensions.get('window').height);
const NavigationStack = createStackNavigator({

    OurMenu : {
        screen: OurMenu,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0
            },
        })
    },
    AllTickets : {
        screen: AllTickets,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0
            },
        })
    },
    Home : {
        screen: Home,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0
               },
        })
    },
    DetailsScreen : {
        screen: DetailsScreen,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0
            },
        })
    },
    OrderDetails : {
        screen: OrderDetails,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0
            },
        })
    },
    Login : {
        screen: Login,
        navigationOptions: ({navigation}) => ({
            title: null,
            header:null
        })
    },
   RecapOrder : {
        screen: RecapOrder,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight  navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0
            },
        })
    },
    CheckoutScreen : {
        screen: CheckoutScreen,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight  navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0
            },
        })
    },
   RecapCheckout : {
        screen: RecapCheckout,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight  navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',

            },
        })
    },
    AccountScreen : {
        screen: AccountScreen,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight  navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0

            },
        })
    },
   Contact : {
        screen: Contact,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight  navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0

            },
        })
    },
    OurAdventure : {
        screen: OurAdventure,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0
            },
        })
    },
    FAQ : {
        screen: FAQ,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0
            },
        })
    },
   PersonalInfos : {
        screen: PersonalInfos,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight  navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',
                elevation:0

            },
        })
    },

    EndCheckout : {
        screen: EndCheckout,
        navigationOptions: ({navigation}) => ({
            title: null,
            headerTitleStyle: { color: 'green' },

            headerLeft : <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight  navigation={navigation}/>,
            headerStyle: { height: 70,
                backgroundColor: '#FFF',

            },
        })
    },





}, {headerMode: 'screen'})
const DrawerNavigator = createDrawerNavigator({
        Stack: NavigationStack,

    },
    {

        contentComponent: DrawerContent,

        drawerPosition: 'left',
        drawerWidth: width - 50,


    });




const Container = createAppContainer(DrawerNavigator);
export default Container;
