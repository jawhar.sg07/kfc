// Imports: Dependencies
import {applyMiddleware, combineReducers, createStore, compose} from "redux";
import {reducer as formReducer} from 'redux-form';

import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist'
import thunk from "redux-thunk";
import promiseMiddleware from 'redux-promise-middleware';
import AsyncStorage from '@react-native-community/async-storage';

// Imports: Redux Root Reducer
import auth from '../reducers/auth';
import appReducer from '../reducers/appReducer';

const persistConfig = {
    key: 'root2',
    storage: AsyncStorage,
    blacklist:['auth']
}
const list=combineReducers({auth,appReducer,form:formReducer})
// Middleware: Redux thunk
const Middlewares = [
    promiseMiddleware,
    thunk
];
const persistedReducer = persistReducer(persistConfig, list)

// Redux: Store
export  const store = createStore(
   persistedReducer,undefined,
    applyMiddleware(...Middlewares),
);
export const persistor = persistStore(store);


