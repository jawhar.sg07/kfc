export function loading(loading) {
    return {type: 'APP_LOADING', loading};
}
export function check(check) {
    return {type: 'APP_CHECK', check};
}
export function menu(menu) {
    return {type: 'APP_MENU', menu};
}
export function indexed(indexed) {
    return {type: 'APP_INDEXED',indexed};
}
export function payments(payments) {
    return {type: 'APP_PAYMENTS', payments};
}
export function collections(collections) {
    return {type: 'APP_COLLECTIONS', collections};
}

export function profile(profile) {
    return {type: 'APP_PROFILE', profile};
}
export function way(way) {
    return {type: 'APP_WAY', way};
}
export function point(point) {
    return {type: 'APP_POINT', point};
}
export function eat(eat) {
    return {type: 'APP_EAT',eat};
}
export function orders(orders) {
    return {type: 'APP_ORDERS',orders};
}
export function clear(clear) {
    return {type: 'APP_CLEAR',clear};
}
export function solde(solde) {
    return {type: 'APP_SOLDE',solde};
}
export function single(single) {
    return {type: 'APP_SINGLE',single};
}
export function sandwich(sandwich) {
    return {type: 'APP_SANDWICH',sandwich};
}
export function soda(soda) {
    return {type: 'APP_SODA',soda};
}
export function frite(frite) {
    return {type: 'APP_FRITE',frite};
}
export function sauce(sauce) {
    return {type: 'APP_SAUCE',sauce};
}
export function sandwich_type(sandwich_type) {
    return {type: 'APP_SANDWICH_TYPE',sandwich_type};
}
export function soda_type(soda_type) {
    return {type: 'APP_SODA_TYPE',soda_type};
}
export function frite_type(frite_type) {
    return {type: 'APP_FRITE_TYPE',frite_type};
}
export function sauce_type(sauce_type) {
    return {type: 'APP_SAUCE_TYPE',sauce_type};
}
export function splice_sauce(splice_sauce) {
    return {type: 'APP_SPLICE_SAUCE',splice_sauce};
}
export function card(card) {
    return {type: 'APP_CARD',card};
}
export function deleteSauce(deleteSauce) {
    return {type: 'APP_DELETE',deleteSauce};
}
export function first(first) {
    return {type: 'APP_FIRST',first};
}
export function place(place) {
    return {type: 'APP_PLACE',place};
}
export function tot(tot) {
    return {type: 'APP_TOT',tot};
}
export function edit(edit) {
    return {type: 'APP_EDIT',edit};
}
export function one(one) {
    return {type: 'APP_ONE',one};
}
export function share(share) {
    return {type: 'APP_SHARE',share};
}
export function deal(deal) {
    return {type: 'APP_DEAL',deal};
}

export function ticket(ticket) {
    return {type: 'APP_TICKET',ticket};
}
export function tickets(tickets) {
    return {type: 'APP_TICKETS',tickets};
}






