import client from '../utlis/dataClient';
import axios from 'axios';
import {clear, first, loading, orders, point, profile, single, solde, ticket, tickets, tot, way} from './app';
import {Alert} from 'react-native';
import moment from 'moment';

export const payOrder = (purchases, navigation,tot) => dispatch => {
    let guid = Math.random().toString(8).substring(2, 8);
    let date = moment(new Date()).format();

    let endArray = {};
    endArray.reference = 'KFC-' + `${guid}`;
    endArray.date = date;
    endArray.data = purchases.cardData;
    endArray.total=tot
    console.log('ed,d,d,ar', endArray);
    dispatch(ticket(endArray))
    dispatch(tickets(endArray))
    navigation.navigate('EndCheckout')
};
export const getSolde = () => dispatch => {
    dispatch(loading(true));
    const headers = {'X-Auth-Token': '4KAJaXfw1gS2M4rlzXhm6wdIJIH+/97PBxwsVS4Q9UeWYTYKlwOPlP0aEJFtIs35gEI='};


    return client.get('bow/getSolde', {headers})
        .then((response) => {
            console.log('reponse solde', response);
            dispatch(solde(response.data.response.solde));
            dispatch(loading(false));
        })
        .catch((err) => {
            console.log('errrr', err.response);
            dispatch(loading(false));
        });
};

function swapArrayLocs(arr, index1, index2) {
    var temp = arr[index1];

    arr[index1] = arr[index2];
    arr[index2] = temp;
}

export const goToSingle = (one, navigation, old_index, item) => dispatch => {

    swapArrayLocs(one, 0, old_index)
    ;
    dispatch(first(one));
    //dispatch(single(one))


    navigation.navigate('DetailsScreen');
    // dispatch(single(newArr))
};
export const backHome=(navigation)=>dispatch=>{

    navigation.navigate('OurMenu')

    dispatch(way(null));
   dispatch(point(null))
    dispatch(tot(0))
    dispatch(ticket([]))
    dispatch(clear([]))

}

