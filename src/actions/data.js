import {
    clear,
    collections,
    frite, frite_type,
    indexed,
    loading,
    menu,
    orders,
    payments,
    point,
    profile,
    sandwich, sandwich_type, sauce, sauce_type,
    soda, soda_type,
    way, splice_sauce, card, deleteSauce, first, place, tot, edit, one,
} from './app';
import client from '../utlis/dataClient';


const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');

// This sets the mock adapter on the default instance
const mock = new MockAdapter(axios);

export const getMenu = () => dispatch => {
    dispatch(loading(true));
    // arguments for reply are (status, data, headers)
    mock.onGet('/allMeals').reply(200, {
        Menu: [
            {
                id: 1,
                name: 'FOR ONE',
                meals: [ {
                    id: 1,
                    name: 'Maxi Meal',
                    price: '17.000',
                    description: '3 pieces chicken Spicy or Original recipe, small coleslaw, medium fries 1 bread',
                    img: require('../assets/images/f1.jpg'),

                },
                    {
                        id: 2,
                        name: 'Crispy Strips',
                        price: '17.000',
                        description: '5 strips original or spicy, small coleslaw, medium fries, 1 bread roll, served with',
                        img: require('../assets/images/f2.jpg'),

                    },
                    {
                        id: 3,
                        name: 'Twister Menu',
                        price: '12.000',
                        description: '2 pièces chicken strips original or spicy recipe, lettuce, tomato, pickle mayo,',
                        img: require('../assets/images/f3.jpg'),

                    },
                    {
                        id: 4,
                        name: 'Zinger Menu',
                        price: '13.500',
                        description: '1 chicken breast spicy, lettuce, cheddar, chili sauce, medium frie',
                        img: require('../assets/images/f4.jpg'),

                    },
                    {
                        id: 5,
                        name: 'Zinger Sandwich',
                        price: '9',
                        description: '1 chicken breast spicy, lettuce, cheddar, chili sauce, medium frie',
                        img: require('../assets/images/f5.jpg'),

                    },
                    {
                        id: 6,
                        name: 'Zinger Box',
                        price: '9',
                        description: '1 chicken breast spicy, lettuce, cheddar, chili sauce , fries, 1 piece',
                        img: require('../assets/images/f6.jpg'),

                    },


                        ],


            },
            {
                id: 2,
                name: 'SHARING',
                meals: [ {
                    id: 1,
                    name: 'Duo Bucket',
                    price: '28.000',
                    description: '4pc of chicken + 4 strips + 2 sodas + 2p fries',
                    img: require('../assets/images/f1.jpg'),

                },
                    {
                        id: 2,
                        name: 'Trio Bucket',
                        price: '45.000',
                        description: '9 pc chicken + 3bread roll +3 p fries + large salade +1l soda',
                        img: require('../assets/images/f2.jpg'),

                    },
                    {
                        id: 3,
                        name: 'Party Bucket',
                        price: '69.000',
                        description: '2 pièces chicken strips original or spicy recipe, lettuce, tomato, pickle mayo,',
                        img: require('../assets/images/f3.jpg'),

                    },
                    {
                        id: 4,
                        name: 'Zinger Menu',
                        price: '13.500',
                        description: '15 pieces Chicken , 5 breads roll, 5 medium  fries, 1 large coleslaw, Soda 1,5L',
                        img: require('../assets/images/f4.jpg'),

                    },


                ],

            },
            {
                id: 3,
                name: 'DEALS',
                meals: [
                ],

            },

        ],
    });

    axios.get('/allMeals').then((response) => {
        console.log('rsponse', response.data.Menu);

        dispatch(menu(response.data.Menu));
        dispatch(loading(false));

    });
};
export const getPaymentMethods = () => dispatch => {
    dispatch(loading(true));

    // arguments for reply are (status, data, headers)
    mock.onGet('/Methods').reply(200, {
        Methods: [
            {
                id: 1,
                label: 'Pay in store',
                sub: [],
            },

            {
                id: 2,
                label: 'Pay Online',
                sub: [
                    {
                        id: 1,
                        label: 'EDINAR',
                        payId: 2,
                        image: require('../assets/images/dinar.png'),

                    },
                    {
                        id: 2,
                        label: 'MASTER CARD',
                        payId: 2,
                        image: require('../assets/images/master.png'),

                    },
                    {
                        id: 2,
                        label: 'VISA CARD',
                        payId: 2,
                        image: require('../assets/images/visa.png'),

                    },
                ],

            },

        ],
    });

    axios.get('/Methods').then((response) => {
        console.log('rsponse methods', response.data.Methods);

        dispatch(payments(response.data.Methods));
        dispatch(loading(false));

    });
};
export const getCollections = () => dispatch => {
    dispatch(loading(true));

    // arguments for reply are (status, data, headers)
    mock.onGet('/Collection').reply(200, {
        Collections: [
            {
                id: 1,
                label: 'IN STORE',
                image: require('../assets/images/c1.png'),

            },

            {
                id: 2,
                label: 'DRIVE THRU',
                image: require('../assets/images/c2.png'),


            },
            {
                id: 3,
                label: 'CRUBSIDE',
                image: require('../assets/images/c3.png'),


            },

        ],
    });

    axios.get('/Collection').then((response) => {
        console.log('rsponse methods', response.data.Collections);
        dispatch(collections(response.data.Collections));


    });
};
export const changeIndex = (index) => dispatch => {
    dispatch(indexed(index));
};
export const saveProfileInformation = (payload) => dispatch => {
    console.log('jfjfj', payload);
    dispatch(profile(payload));
};
export const saveWay = (pay, second) => dispatch => {
    console.log('wayy', pay);
    console.log('secode', second);
    if (pay.sub.length === 0) {
        dispatch(way(pay));
    } else {
        dispatch(way(pay.sub[second]));
    }
};
export const savePoint = (col) => dispatch => {
    console.log('colection point', col);
    dispatch(point(col));
};
export const saveEat = (local) => dispatch => {
    console.log('eatttttt', local);
};
export const clearCard = (navigation) => dispatch => {
    dispatch(point(null));
    dispatch(way(null));
    dispatch(profile(null));
    navigation.navigate('OurMenu');
    dispatch(clear([]));
};
export const resetOrder = () => dispatch => {
    dispatch(point(null));

};
export const addToCard = (data, navigation) => dispatch => {

    dispatch(orders(data));
    navigation.navigate('RecapOrder');
    //dispatch(clear([]))
};
export const getSandwichType = () => dispatch => {
    dispatch(loading(true));

    // arguments for reply are (status, data, headers)
    mock.onGet('/sandwich').reply(200, {
        sandwich:
            {
                id: 1,
                label: 'Type Sandwich',
                sub: [
                    {
                        id: 1,
                        label: 'original',


                    },
                    {
                        id: 2,
                        label: 'Epicé',


                    },
                    {
                        id: 3,
                        label: 'MIXED',


                    },

                ],


            },


    });

    axios.get('/sandwich').then((response) => {
        console.log('rsponse methods', response.data.sandwich);

        dispatch(sandwich(response.data.sandwich));
        dispatch(loading(false));

    });
};
export const getSodaType = () => dispatch => {
    dispatch(loading(true));

    // arguments for reply are (status, data, headers)
    mock.onGet('/soda').reply(200, {
        soda:
            {
                id: 1,
                label: 'Soda',
                sub: [
                    {
                        id: 1,
                        label: 'Cocacola',
                        image: require('../assets/images/soda1.jpg'),


                    },
                    {
                        id: 2,
                        label: 'Coca Zéro',
                        image: require('../assets/images/soda2.jpg'),


                    },
                    {
                        id: 3,
                        label: 'Boga Lime',
                        image: require('../assets/images/soda3.jpg'),


                    },
                    {
                        id: 4,
                        label: 'Fanta',
                        image: require('../assets/images/soda4.jpg'),


                    },
                    {
                        id: 5,
                        label: 'Orangina',
                        image: require('../assets/images/soda5.jpg'),


                    },


                ],


            },


    });

    axios.get('/soda').then((response) => {
        console.log('rsponse soda', response.data);

        dispatch(soda(response.data.soda));

    });
};
export const getFriteType = () => dispatch => {
    dispatch(loading(true));

    // arguments for reply are (status, data, headers)
    mock.onGet('/frite').reply(200, {
        frite:
            {
                id: 1,
                label: 'Frite',
                sub: [
                    {
                        id: 1,
                        label: 'Medium',
                        image: require('../assets/images/friteL.jpg'),
                        price:0


                    },
                    {
                        id: 2,
                        label: 'Large',
                        image: require('../assets/images/friteL.jpg'),
                        price: 1,


                    },


                ],


            },


    });

    axios.get('/frite').then((response) => {
        dispatch(frite(response.data.frite));

    });
};
export const getSauceType = () => dispatch => {
    dispatch(loading(true));

    // arguments for reply are (status, data, headers)
    mock.onGet('/sauce').reply(200, {
        sauce:
            {
                id: 1,
                label: 'Dipping Sauce',
                price: 1,
                sub: [
                    {
                        id: 1,
                        label: 'Mayo',
                        image: require('../assets/images/sauce1.jpg'),
                        price: 1,


                    },
                    {
                        id: 2,
                        label: 'Ketchup',
                        image: require('../assets/images/sauce2.jpg'),
                        price: 1,


                    },
                    {
                        id: 3,
                        label: 'BBQ',
                        image: require('../assets/images/sauce3.jpg'),
                        price: 1,


                    },


                ],


            },


    });

    axios.get('/sauce').then((response) => {
        dispatch(sauce(response.data.sauce));

    });
};
export const getCard = () => dispatch => {
    dispatch(loading(true));
    // arguments for reply are (status, data, headers)
    mock.onGet('/allCard').reply(200, {
        menuItems: [
            {
                id: 1,
                name: 'Boxes',
                image: require('../assets/images/list6.png'),
                meals: [{
                    id: 1,
                    name: 'Crispy Stripes Box ',
                    fam: 'Boxes',
                    product: 'Crispy Stripes Box',
                    price: '13.000',
                    transactionNumber: '12',
                    barCode: '123456789',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger,plus spine-tingling zing',
                    img: require('../assets/images/item3.png'),

                }, {
                    id: 2,
                    name: 'Snack Box',
                    fam: 'boxes',
                    product: 'Snack Box',
                    price: '14.000',
                    transactionNumber: '12',
                    barCode: '99999999',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger',
                    img: require('../assets/images/item2.png'),
                },
                    {
                        id: 3,
                        name: 'Wings Box ',
                        fam: 'Boxes',
                        price: '17.000',
                        product: 'Wings Box',
                        transactionNumber: '12',
                        barCode: '111111111',
                        paimentType: 'voucher',
                        description: 'Our delicious,succulent 100% chicken breast fillet burger',
                        img: require('../assets/images/item1.png'),
                    },
                    {
                        id: 4,
                        name: 'Zinger Box',
                        fam: 'Boxes',
                        price: '17.000',
                        product: 'Zinger Box',
                        transactionNumber: '12',
                        barCode: '111111111',
                        paimentType: 'voucher',
                        description: 'Our delicious,succulent 100% chicken breast fillet burger',
                        img: require('../assets/images/item1.png'),
                    },
                    {
                        id: 5,
                        name: ' Mighty Zinger Box',
                        fam: 'Boxes',
                        price: '20.5000',
                        product: 'Mighty Zinger Box',
                        transactionNumber: '12',
                        barCode: '111111111',
                        paimentType: 'voucher',
                        description: 'Our delicious,succulent 100% chicken breast fillet burger',
                        img: require('../assets/images/item1.png'),
                    },


                ],
            },
            {
                id: 2,
                name: 'Buckets',
                image: require('../assets/images/list8.png'),
                meals: [{
                    id: 1,
                    name: 'Duo Bucket ',
                    fam: 'Buckets',
                    product: 'Duo Bucket',
                    price: '28.000',
                    transactionNumber: '12',
                    barCode: '123456789',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger,plus spine-tingling zing',
                    img: require('../assets/images/item3.png'),

                }, {
                    id: 2,
                    name: 'Trio Bucket',
                    fam: 'Buckets',
                    product: 'Trio Bucket',
                    price: '45.000',
                    transactionNumber: '12',
                    barCode: '99999999',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger',
                    img: require('../assets/images/item2.png'),
                },
                    {
                        id: 3,
                        name: 'party Bucket',
                        fam: 'Buckets',
                        price: '69.000',
                        product: 'party Bucket',
                        transactionNumber: '12',
                        barCode: '111111111',
                        paimentType: 'voucher',
                        description: 'Our delicious,succulent 100% chicken breast fillet burger',
                        img: require('../assets/images/item1.png'),
                    },
                    {
                        id: 4,
                        name: 'Crazy Bucket',
                        fam: 'Buckets',
                        price: '42.000',
                        product: 'Crazy Bucket',
                        transactionNumber: '12',
                        barCode: '111111111',
                        paimentType: 'voucher',
                        description: 'Our delicious,succulent 100% chicken breast fillet burger',
                        img: require('../assets/images/item3.png'),
                    },


                ],
            },
            {
                id: 3,
                name: 'Meals',
                image: require('../assets/images/list1.png'),
                meals: [{
                    id: 1,
                    name: 'Hot Shots Meal ',
                    fam: 'Meals',
                    product: 'Hot Shots Meal',
                    price: '16.000',
                    transactionNumber: '12',
                    barCode: '123456789',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger,plus spine-tingling zing',
                    img: require('../assets/images/item3.png'),

                }, {
                    id: 2,
                    name: 'Maxi Meal',
                    fam: 'Meals',
                    product: 'Maxi Meal',
                    price: '17.000',
                    transactionNumber: '12',
                    barCode: '99999999',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger',
                    img: require('../assets/images/item2.png'),
                },
                    {
                        id: 3,
                        name: 'Crispy Strips Meal',
                        fam: 'Meals',
                        price: '17.000',
                        product: 'Crispy Strips Meal',
                        transactionNumber: '12',
                        barCode: '111111111',
                        paimentType: 'voucher',
                        description: 'Our delicious,succulent 100% chicken breast fillet burger',
                        img: require('../assets/images/item1.png'),
                    },


                ],
            },
            {
                id: 4,
                name: 'Sandwich',
                image: require('../assets/images/list7.png'),
                meals: [{
                    id: 1,
                    name: 'Twister Sandwich',
                    fam: 'Sandwich',
                    product: 'Twister Sandwich',
                    price: '7.500',
                    transactionNumber: '12',
                    barCode: '123456789',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger,plus spine-tingling zing',
                    img: require('../assets/images/item3.png'),

                }, {
                    id: 2,
                    name: 'Twister Menu',
                    fam: 'Sandwich',
                    product: 'FILLET TOWER',
                    price: '12.000',
                    transactionNumber: '12',
                    barCode: '99999999',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger',
                    img: require('../assets/images/item2.png'),
                },
                    {
                        id: 3,
                        name: 'Mighty Zinger Sandwich',
                        fam: 'Sandwich',
                        price: '13.000',
                        product: 'BBQ BACON ',
                        transactionNumber: '12',
                        barCode: '111111111',
                        paimentType: 'voucher',
                        description: 'Our delicious,succulent 100% chicken breast fillet burger',
                        img: require('../assets/images/item1.png'),
                    },
                    {
                        id: 4,
                        name: 'Zinger Sandwich',
                        fam: 'Sandwich',
                        price: '9.000',
                        product: 'Zinger Sandwich ',
                        transactionNumber: '12',
                        barCode: '111111111',
                        paimentType: 'voucher',
                        description: 'Our delicious,succulent 100% chicken breast fillet burger',
                        img: require('../assets/images/item1.png'),
                    },


                ],
            },
            {
                id: 5,
                name: 'Kids Meals',
                image: require('../assets/images/list6.png'),
                meals: [{
                    id: 1,
                    name: 'Kids Meal Chicken ',
                    fam: 'Kids Meals',
                    product: 'Kids Meal Chicken',
                    price: '10.000',
                    transactionNumber: '12',
                    barCode: '123456789',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger,plus spine-tingling zing',
                    img: require('../assets/images/item3.png'),

                }, {
                    id: 2,
                    name: 'Kids Meal Strips',
                    fam: 'Kids Meals',
                    product: 'Kids Meal Strips',
                    price: '10.00',
                    transactionNumber: '12',
                    barCode: '99999999',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger',
                    img: require('../assets/images/item2.png'),
                },


                ],
            },
            {
                id: 6,
                name: 'Sides',
                image: require('../assets/images/list5.png'),
                meals: [{
                    id: 1,
                    name: 'Hot Shots',
                    fam: 'Sides',
                    product: 'Hot Shots',
                    price: '11.900',
                    transactionNumber: '12',
                    barCode: '123456789',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger,plus spine-tingling zing',
                    img: require('../assets/images/item3.png'),

                }, {
                    id: 2,
                    name: 'Hot Wings Bucket',
                    fam: 'Sides',
                    product: 'Hot Wings Bucket',
                    price: '35.000',
                    transactionNumber: '12',
                    barCode: '99999999',
                    paimentType: 'voucher',
                    description: 'Our delicious,succulent 100% chicken breast fillet burger',
                    img: require('../assets/images/item2.png'),
                },
                    {
                        id: 3,
                        name: 'Hot Wings',
                        fam: 'Sides',
                        price: '5.000',
                        product: 'Hot Wings',
                        transactionNumber: '12',
                        barCode: '111111111',
                        paimentType: 'voucher',
                        description: 'Our delicious,succulent 100% chicken breast fillet burger',
                        img: require('../assets/images/item1.png'),
                    },


                ],
            },


        ],
    });

    axios.get('/allCard').then((response) => {
        console.log('rsponse', response.data.menuItems);

        dispatch(first(response.data.menuItems));

    });
};

export const getOne = () => dispatch => {
    dispatch(loading(true));
    // arguments for reply are (status, data, headers)
    mock.onGet('/for').reply(200, {
        forOne: [
            {
                id: 1,
                name: 'Maxi Meal',
                price: '17.000',
                description: '3 pieces chicken Spicy or Original recipe, small coleslaw, medium fries 1 bread',
                img: require('../assets/images/f1.jpg'),

            },
            {
                id: 2,
                name: 'Crispy Strips',
                price: '17.000',
                description: '5 strips original or spicy, small coleslaw, medium fries, 1 bread roll, served with',
                img: require('../assets/images/f2.jpg'),

            },
            {
                id: 3,
                name: 'Twister Menu',
                price: '12.000',
                description: '2 pièces chicken strips original or spicy recipe, lettuce, tomato, pickle mayo,',
                img: require('../assets/images/f3.jpg'),

            },
            {
                id: 4,
                name: 'Zinger Menu',
                price: '13.500',
                description: '1 chicken breast spicy, lettuce, cheddar, chili sauce, medium frie',
                img: require('../assets/images/f4.jpg'),

            },
            {
                id: 5,
                name: 'Zinger Sandwich',
                price: '9',
                description: '1 chicken breast spicy, lettuce, cheddar, chili sauce, medium frie',
                img: require('../assets/images/f5.jpg'),

            },
            {
                id: 6,
                name: 'Zinger Box',
                price: '9',
                description: '1 chicken breast spicy, lettuce, cheddar, chili sauce , fries, 1 piece',
                img: require('../assets/images/f6.jpg'),

            },
            ]
    });

    axios.get('/for').then((response) => {
        console.log('rsponse', response.data.forOne);
        dispatch(one(response.data.forOne))



    });
};
export const sandwichType = (type) => dispatch => {
    dispatch(sandwich_type(type));
};
export const sodaType = (type) => dispatch => {
    dispatch(soda_type(type));
};
export const friteType = (type) => dispatch => {
    dispatch(frite_type(type));
};

export const sauceType = (type) => dispatch => {
    console.log('tyeeoeoeoe', type);
    dispatch(sauce_type(type));
};
export const SliceSauce = (type) => dispatch => {
    console.log('typeeeeeeeeee', type);
    dispatch(splice_sauce(type));
};

export const submitPurchase = (qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot) => dispatch => {
    console.log(name, sandwichType, sodaType, friteType, sauceType);
    let Arr = {};
    Arr.product = name;
    Arr.type = sandwichType;
    Arr.soda = sodaType;
    Arr.frite = friteType;
    Arr.sauce = sauceType;
    Arr.quantity = qty;


    //let total=ex_tot+(Number(Arr.product.price)*Arr.quantity)
    dispatch(card([Arr]));
    dispatch(deleteSauce([]));
    dispatch(soda_type({}));
    dispatch(frite_type({}));
    dispatch(sandwich_type({}));
    navigation.navigate('RecapOrder');
//dispatch(tot(total))

};
export const calculateTotal = (card) => dispatch => {

    let total = 0;
    card.cardData.forEach((item) => {
        for (let i = 0; i < item.length; i++) {
            let sum = ((Number(item[0].product?.price)) +
                (item[0].sauce.sauces[0] ? item[0].sauce.sauces[0].price : 0) +
                (item[0].frite.label === 'Large' ? (item[0].frite.price) : 0)) * item[0].quantity;
            total += sum;

        }
    });
    dispatch(tot(total));

};
export const getPlaces = () => dispatch => {


    // arguments for reply are (status, data, headers)
    mock.onGet('/places').reply(200, {
        places: [
            {
                id: 1,
                label: 'KFC La Marsa',
                description: 'Avenue Habib Bourguiba, La Marsa Plage Marsa',
                openTime: '8H',
                closeTime: '22H',
                longitude: '36.8840904',
                latitude: '10.3301157',

            },
            {
                id: 1,
                label: 'KFC Mall Sousse',
                description: 'Mall Of Sousse RN1, Tunis Km 128. 4060, 4060',
                openTime: '8H',
                closeTime: '22H',
                longitude: '35.9035589',
                latitude: '10.5415252',

            },


        ],


    });

    axios.get('/places').then((response) => {


        dispatch(place(response.data.places));

    });
};
export const changeQuantity = (index, item, qte, all) => dispatch => {

    let all_array = all.cardData;
    let array = all.cardData[index];

    let new_item = item;
    new_item.quantity = qte;

    array = [new_item];

    all_array[index] = array;

    dispatch(edit(all_array));

};
export const deleteOrder = (index, item, all) => dispatch => {
    console.log('item delete', item);
    console.log('all delete', all);
    let all_delete=all.cardData
    all_delete.splice(index,1)
    console.log('deleted item',all_delete);
    dispatch(edit(all_delete))

};
