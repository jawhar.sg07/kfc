import {combineReducers} from 'redux';

const initialUserState = {

    tickets:[]

};
export function check(state = null, action) {
    if (action.type === 'APP_CHECK') {
        return action.check;
    } else {
        return state;
    }
}
export function menu(state =[], action) {
    if (action.type === 'APP_MENU') {
        return action.menu;
    } else {
        return state;
    }
}
export function payments(state =[], action) {
    if (action.type === 'APP_PAYMENTS') {
        return action.payments;
    } else {
        return state;
    }
}
export function collections(state =[], action) {
    if (action.type === 'APP_COLLECTIONS') {
        return action.collections;
    } else {
        return state;
    }
}
export function profile(state =null, action) {
    if (action.type === 'APP_PROFILE') {
        return action.profile;
    } else {
        return state;
    }
}
export function way(state =null, action) {
    if (action.type === 'APP_WAY') {
        return action.way;
    } else {
        return state;
    }
}
export function point(state =null, action) {
    if (action.type === 'APP_POINT') {
        return action.point;
    } else {
        return state;
    }
}
export function eat(state =null, action) {
    if (action.type === 'APP_EAT') {
        return action.eat;
    } else {
        return state;
    }
}
export function solde(state =0, action) {
    if (action.type === 'APP_SOLDE') {
        return action.solde;
    } else {
        return state;
    }
}
export function tickets(state = initialUserState, action) {

    if (action.type === 'APP_TICKETS') {
        return {

            tickets: [...state.tickets, action.tickets],

        };
    } else {
        return state;
    }
}





export default combineReducers({ check,menu,payments,collections,profile,way,point,eat,solde,tickets});
