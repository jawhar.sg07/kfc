import {combineReducers} from 'redux';

const initialUserState = {
    arr: [],
    sauces: [],
    cardData:[],
    firstData:[],
    nextData:[],
    tickets:[]

};

export function loading(state = false, action) {
    if (action.type === 'APP_LOADING') {
        return action.loading;
    } else {
        return state;
    }
}

export function indexed(state = 0, action) {
    if (action.type === 'APP_INDEXED') {
        return action.indexed;
    } else {
        return state;
    }
}

export function orders(state = initialUserState, action) {
    console.log('initial state', state);
    if (action.type === 'APP_ORDERS') {
        return {
            ...state,
            arr: [...state.arr, action.orders],

        };
    } else if (action.type === 'APP_CLEAR') {
        return {
            ...state,
            arr: action.clear,

        };
    } else {
        return state;
    }
}

export function single(state = [], action) {
    if (action.type === 'APP_SINGLE') {
        return action.single;
    } else {
        return state;
    }
}

export function sandwich(state = {}, action) {
    if (action.type === 'APP_SANDWICH') {
        return action.sandwich;
    } else {
        return state;
    }
}

export function soda(state = {}, action) {
    if (action.type === 'APP_SODA') {
        return action.soda;
    } else {
        return state;
    }
}

export function frite(state = {}, action) {
    if (action.type === 'APP_FRITE') {
        return action.frite;
    } else {
        return state;
    }
}

export function sauce(state = {}, action) {
    if (action.type === 'APP_SAUCE') {
        return action.sauce;
    } else {
        return state;
    }
}

export function sandwich_type(state = {}, action) {
    if (action.type === 'APP_SANDWICH_TYPE') {
        return action.sandwich_type;
    } else {
        return state;
    }
}

export function soda_type(state = {}, action) {
    if (action.type === 'APP_SODA_TYPE') {
        return action.soda_type;
    } else {
        return state;
    }
}

export function frite_type(state = {}, action) {
    if (action.type === 'APP_FRITE_TYPE') {
        return action.frite_type;
    } else {
        return state;
    }
}

export function sauce_type(state = initialUserState, action) {

    if (action.type === 'APP_SAUCE_TYPE') {
        return {

           sauces: [...state.sauces, action.sauce_type],

        };
    } else if(action.type === 'APP_SPLICE_SAUCE'){
        console.log('item from tye',action);
        return {

            sauces:[...state.sauces.filter((item)=>item!==action.splice_sauce)]

        };

    } else if (action.type === 'APP_DELETE') {
        return {
            ...state,
           sauces: action.deleteSauce,

        };
    }else {
        return state;
    }
}
export function card(state = initialUserState, action) {
    console.log('actionnncard',action);
    if (action.type === 'APP_CARD') {

        return {
            cardData: [...state.cardData,action.card]
        }
    } else if (action.type === 'APP_CLEAR') {
        return {
            ...state,
           cardData: action.clear,

        };
    } else if(action.type==='APP_EDIT'){

        return {
            cardData: action.edit
        }
    } else {
        return state;
    }
}
export function first(state = [], action) {

    if (action.type === 'APP_FIRST') {

        return action.first;
    } else {
        return state;
    }
}
export function place(state = [], action) {

    if (action.type === 'APP_PLACE') {

        return action.place;
    } else {
        return state;
    }
}
export function tot(state = 0, action) {

    if (action.type === 'APP_TOT') {

        return action.tot;
    } else {
        return state;
    }
}
export function nextt(state = initialUserState, action) {

    if (action.type === 'APP_ONE') {
        return {

           nextData: action.one,

        };
    } else if (action.type === 'APP_SHARE') {
        return {

           nextData: action.share

        };
    } else if (action.type === 'APP_DEAL') {
        return {

            nextData: action.deal

        };
    }else {
        return state;
    }
}
export function ticket(state = [], action) {

    if (action.type === 'APP_TICKET') {

        return action.ticket;
    } else {
        return state;
    }
}


export default combineReducers({
    tot,
    ticket,

    place,
    first,
    loading,
    indexed,
    orders,
    single,
    sandwich,
    soda,
    frite,
    sauce,
    sandwich_type,
    soda_type,
    frite_type,
    sauce_type,
    card,nextt
});
