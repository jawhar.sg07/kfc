// Imports: Dependencies
import React, {useEffect, useState,useRef} from 'react';
import {
    Text,
    TouchableOpacity,
    View,
    Button,
    Image,
    ScrollView,
    ActivityIndicator,
    Alert,
    TouchableWithoutFeedback,
    TouchableHighlight,
} from 'react-native';

import Scroll from'../assets/images/scrollTop.svg'

import {connect} from 'react-redux';
import PickFile from '../components/pickFileComponent';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import Phone from '../assets/images/phone.svg';
import Sms from '../assets/images/mail (1).svg';
import Location from '../assets/images/location.svg';
import {calculateTotal, changeIndex, getMenu} from '../actions/data';
import {FloatingLabelInput} from 'react-native-floating-label-input';


import ServiceType from '../components/dropDownMatiere';



const Contact = (props) => {
    const scrollRef = useRef();
    const [birthday, setBirthday] = useState('');
    const [phone, setPhone] = useState('');
    const [message, setMessage] = useState('');
    const [visible,setVisible]=useState(false)

    useEffect(() => {

    }, []);
    const  handleOnScroll = event => {
        const offsetY = event.nativeEvent.contentOffset.y;

        if (offsetY > 50) {
          setVisible(true)
        } else {
          setVisible(false)
        }
    };
    const onPressTouch = () => {
        scrollRef.current?.scrollTo({
            y: 0,
            animated: true,
        });
    }
    return (


        <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
            <MenuBar {...props}/>
            <ScrollView style={{display: 'flex', flex: 1}} ref={scrollRef} onScroll={event => handleOnScroll(event)} >
                <View style={styles.adventureView}>
                    <Text style={styles.adventureTitle}>NOUS CONTACTER</Text>

                </View>
                <View style={{
                    borderWidth: 0.7,
                    borderColor: '#E1E1E1',
                    marginHorizontal: 15,
                    marginBottom: 20,
                    padding: 15,
                    borderRadius: 4,
                }}>
                    <Text style={{color: 'black', fontSize: 14, fontFamily: 'Montserrat-Bold', marginBottom: 20}}>INFORMATIONS
                        SUR LE MAGASIN</Text>
                    <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', marginBottom: 15}}>
                        <Location/>
                        <Text style={{
                            color: '#726F6F',
                            marginLeft: 10,
                            fontFamily: 'Montserrat-SemiBold',
                            fontSize: 13,
                        }}>Tunisie</Text>
                    </View>
                    <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', marginBottom: 15}}>
                        <Phone/>
                        <Text
                            style={{color: '#726F6F', marginLeft: 10, fontFamily: 'Montserrat-SemiBold', fontSize: 13}}>Appelez-nous
                            : +216 55123456</Text>
                    </View>
                    <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', marginBottom: 15}}>
                        <Sms/>
                        <Text
                            style={{color: '#726F6F', marginLeft: 10, fontFamily: 'Montserrat-SemiBold', fontSize: 13}}>Écrivez-nous
                            : contact@dotit-corp.com</Text>
                    </View>
                </View>
                <ServiceType/>
                <FloatingLabelInput
                    containerStyles={{
                        backgroundColor: '#FFF', marginHorizontal: 10, borderRadius: 4,
                        elevation: 4, borderColor: '#00000029', borderWidth: 0.5,
                        marginBottom: 10, height: 70,
                    }}
                    labelStyles={{color: '#000000', fontSize: 14, fontFamily: 'Montserrat-Bold', marginBottom: 10}}
                    inputStyles={{
                        color: '#717070',
                        fontSize: 12,
                        fontFamily: 'Montserrat-Medium',
                    }}
                    label="Adresse e-mail"
                    value={birthday}


                    onChangeText={value => setBirthday(value)}
                />
                <FloatingLabelInput
                    containerStyles={{
                        backgroundColor: '#FFF', marginHorizontal: 10, borderRadius: 4,
                        elevation: 4, borderColor: '#00000029', borderWidth: 0.5,
                        marginBottom: 10, height: 70,
                    }}
                    labelStyles={{color: '#000000', fontSize: 14, fontFamily: 'Montserrat-Bold', marginBottom: 10}}
                    inputStyles={{
                        color: '#717070',
                        fontSize: 12,
                        fontFamily: 'Montserrat-Medium',
                    }}
                    label="Téléphone"
                    value={phone}

                    keyboardType="numeric"
                    onChangeText={value => setPhone(value)}
                />
                <View style={{marginHorizontal: 20, marginTop: 15}}>
                    <Text style={{color: '#726F6F', fontSize: 13, fontFamily: 'Montserrat-SemiBold'}}>Référence de
                        l'achat</Text>
                </View>

                <PickFile/>
                <FloatingLabelInput
                    containerStyles={{
                        backgroundColor: '#FFF', marginHorizontal: 10, borderRadius: 4,
                        elevation: 4, borderColor: '#00000029', borderWidth: 0.5,
                        marginBottom: 10, height: 100,
                    }}
                    labelStyles={{color: '#000000', fontSize: 14, fontFamily: 'Montserrat-Bold', marginBottom: 10}}
                    inputStyles={{
                        color: '#717070',
                        fontSize: 14,
                        fontFamily: 'Montserrat-Medium',
                    }}
                    label="Message"
                    value={message}
                    multiline={true}
                    onChangeText={value => setMessage(value)}
                />

                <TouchableOpacity style={{
                    alignItems: 'center',
                    backgroundColor: '#E3002B',
                    borderRadius: 50,
                    marginHorizontal:15,
                    marginVertical:30,
                    padding: 16,
                }}

                >
                    <Text
                        style={{
                            color: 'white',
                            fontSize: 15,
                            fontFamily: 'Montserrat-Bold',
                        }}

                    >ENVOYER</Text>
                </TouchableOpacity>

            </ScrollView>
            {visible &&

            <TouchableOpacity   style={{ position: 'absolute',
                width: 50,
                height: 50,
                alignItems: 'center',
                backgroundColor:'#E3002B',
                justifyContent: 'center',
                right: 15,
                bottom: 50,}} onPress={()=>onPressTouch()}>
                <Scroll/>
            </TouchableOpacity>

            }
        </View>


    );
};
const mapStateToProps = state => {
    const {loading, indexed, tot, orders, clear, sandwich, place, soda, frite, sauce, sandwich_type, sauce_type, frite_type, soda_type, card} = state.auth;
    const {check, menu, point} = state.appReducer;

    return {
        place,
        loading,
        check,
        tot,
        menu,
        indexed,
        point,
        orders,
        clear,
        sandwich,
        soda,
        frite,
        sauce,
        sandwich_type,
        sauce_type,
        frite_type,
        soda_type,
        card,
    };

};

const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    calculateTotal: (card) => dispatch(calculateTotal(card)),
    changeIndex: (index) => dispatch(changeIndex(index)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
