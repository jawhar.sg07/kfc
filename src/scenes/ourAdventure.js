import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList} from 'react-native';
import MenuBar from '../components/MenuBar';
import {connect} from 'react-redux';
import {
    addToCard, calculateTotal,
    changeIndex,
    friteType,
    getMenu, resetOrder,
    sandwichType,
    sauceType,
    savePoint, SliceSauce,
    sodaType, submitPurchase,
} from '../actions/data';
import styles from '../assets/styles';

const OurAdventure = (props) => {
    return (
        <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
            <MenuBar {...props}/>
            <ScrollView style={{display: 'flex', flex: 1}}>
                <View style={styles.adventureView}>

                    <Text style={styles.adventureTitle}>NOTRE AVENTURE</Text>

                </View>

                <Image style={{height: 150}} source={require('../assets/images/ad1.png')}/>
                <View style={{marginVertical: 15, marginHorizontal: 25}}>
                    <Text style={styles.dateAd}>1930</Text>
                    <Text style={styles.textAd}>APRÈS DE NOMBREUX ÉCHANGES, HARLAND SANDERS A DÉMÉNAGÉ AU KENTUCKY
                        ET A OUVERT UNE STATION-SERVICE AVEC UN RESTAURANT. IL SERT DU POULET CUIT SELON UNE RECETTE
                        SECRÈTE! KENTUCKY FRIED CHICKEN (KFC) EST NÉ!</Text>
                </View>
                <Image source={require('../assets/images/ad2.png')}/>
                <View style={{marginHorizontal: 25}}>
                    <Text style={styles.dateAd}>1935</Text>
                    <Text style={styles.textAd}>LE GOUVERNEUR DU KENTUCKY HONORE HARLAND SANDERS AVEC LE TITRE DE
                        «COLONEL» POUR SA CONTRIBUTION À LA CUISINE AMÉRICAINE PAR LE BIAIS DE SON RESTAURANT.</Text>
                </View>
                <Image source={require('../assets/images/ad3.png')}/>
                <View style={{marginHorizontal: 25, marginVertical: 15}}>
                    <Text style={styles.dateAd}>1955</Text>
                    <Text style={styles.textAd}>65 ANS EST L'ÂGE DE LA RETRAITE. PAS POUR LE COLONEL SANDERS QUI, AVEC
                        TRÈS PEU D'ARGENT, PART À LA CONQUÊTE DU MARCHÉ AMÉRICAIN AVEC SA RECETTE SECRÈTE.</Text>
                </View>
                <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={require('../assets/images/bucket.png')}/>
                </View>
                <View style={{marginHorizontal: 25, marginVertical: 15}}>
                    <Text style={styles.dateAd}>1957</Text>
                    <Text style={styles.textAd}>LE 1ER KFC BUCKET EST NÉ.</Text>
                </View>
                <View style={{marginHorizontal: 25, marginVertical: 15}}>
                    <Text style={styles.dateAd}>1964</Text>
                    <Text style={styles.textAd}>KFC COMPTE PLUS DE 600 RESTAURANTS. LE COLONEL SANDERS VEND SA MARQUE ET
                        EN FAIT LA PROMOTION DANS LE MONDE ENTIER. IL DEVIENT L'EMBLÈME DE KFC.</Text>
                </View>
                <Image source={require('../assets/images/ad6.png')}/>
                <View style={{marginHorizontal: 25, marginVertical: 15}}>
                    <Text style={styles.dateAd}>1976</Text>
                    <Text style={styles.textAd}>LE COLONEL SANDERS QUI EST DEVENU LE VISAGE DE SA MARQUE EST PLACÉ DANS
                        LE SONDAGE COMME DEUXIÈME PERSONNE LA PLUS POPULAIRE AU MONDE.</Text>
                </View>
                <View style={{marginHorizontal: 25, marginVertical: 15}}>
                    <Text style={styles.dateAd}>1979</Text>
                    <Text style={styles.textAd}>KFC VEND PLUS DE 2,7 MILLIARDS DE MORCEAUX DE POULET ET COMPTE 6000
                        RESTAURANTS DANS LE MONDE</Text>
                </View>
                <Image source={require('../assets/images/ad7.png')}/>
                <View style={{marginHorizontal: 25, marginVertical: 15}}>
                    <Text style={styles.dateAd}>1980</Text>
                    <Text style={styles.textAd}>LE COLONEL SANDERS MEURT, MAIS KFC CONTINUE DE SE DÉVELOPPER.</Text>
                </View>
                <View style={{marginHorizontal: 25, marginVertical: 15}}>
                    <Text style={styles.dateAd}>2008</Text>
                    <Text style={styles.textAd}>KFC SE REFAIT UNE BEAUTÉ AVEC UN NOUVEAU LOGO.</Text>
                </View>
                <View style={{marginHorizontal: 25, marginVertical: 15}}>
                    <Text style={styles.dateAd}>2018</Text>
                    <Text style={styles.textAd}>DÉJÀ PRÉSENT DANS 125 PAYS, KFC ARRIVE EN TUNISIE.</Text>
                </View>
                <View style={{marginHorizontal: 25, marginVertical: 15}}>
                    <Text style={styles.TextAbout}>À propos de KFC:</Text>
                    <Text style={styles.textAd}>Réputé depuis plus de 75 ans pour ses célèbres recettes de poulet, KFC
                        compte plus de 20 000 restaurants dans 126 pays. Chaque jour, près de 10 millions de personnes
                        apprécient les produits servis dans les restaurants KFC. En Tunisie, KFC a ouvert son premier
                        restaurant en janvier 2018 aux Berges du Lac 1. Aujourd'hui la marque compte 8 restaurants, dont
                        6 dans le Grand Tunis et 2 à Sousse.</Text>
                </View>
            </ScrollView>
        </View>
    );
};
const mapStateToProps = state => {
    const {loading, indexed, tot, orders, clear, sandwich, place, soda, frite, sauce, sandwich_type, sauce_type, frite_type, soda_type, card} = state.auth;
    const {check, menu, point} = state.appReducer;

    return {
        place,
        loading,
        check,
        tot,
        menu,
        indexed,
        point,
        orders,
        clear,
        sandwich,
        soda,
        frite,
        sauce,
        sandwich_type,
        sauce_type,
        frite_type,
        soda_type,
        card,
    };
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    addToCard: (data, navigation) => dispatch(addToCard(data, navigation)),

    sandwichType: (type) => dispatch(sandwichType(type)),
    sodaType: (type) => dispatch(sodaType(type)),
    friteType: (type) => dispatch(friteType(type)),
    sauceType: (type) => dispatch(sauceType(type)),
    savePoint: (col) => dispatch(savePoint(col)),
    SliceSauce: (type) => dispatch(SliceSauce(type)),
    calculateTotal: (card) => dispatch(calculateTotal(card)),
    resetOrder: () => dispatch(resetOrder()),
    submitPurchase: (qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot) => dispatch(submitPurchase(qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OurAdventure);
