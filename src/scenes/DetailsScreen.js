//screen 2 after choice meals from home
import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import {calculateTotal, changeIndex, getMenu, resetOrder, savePoint} from '../actions/data';
import ModalChooseOrder from '../components/ModalChooseOrder';
import ModalOrderRestaurantDetails from '../components/ModalOrderRestDetails';
import ModalReacapChoice from '../components/ModalRecapChoice';
import ListMeals from '../components/ListMealsForMe';
import Next from '../assets/images/next.svg';
import ListDetails from '../components/ListDetails';
import ModalMap from '../components/ModalMap';
import Place from '../components/place';
const DetailsScreen=(props)=>{
    const [modalVisible, setModalVisible] = useState(false);
    const [modalOrder, setModalOrder] = useState(false);
    const [modalRecap, setModalRecap] = useState(false);
    const [modalMap, setModalMap] = useState(false);
    const openModalMap=()=>{
        setModalMap(true)
    }
    const closeModalMap=()=>{
        setModalMap(false)
    }
    const openModalChoose = () => {
        setModalVisible(true);
    };
    const closeModalChoose = () => {
        setModalVisible(false);
    };
    const openModalOrder = () => {
        setModalOrder(true);
    };
    const closeModalOrder = () => {
        setModalOrder(false);
    };
    const openModalRecap = () => {
        setModalRecap(true);
    };
    const closeModalRecap = () => {
        setModalRecap(false);
    };


    return(
        <View style={styles.homeContainer}>
            <ModalChooseOrder {...props} modalVisible={modalVisible} closeModalVisible={closeModalChoose}
                              closeModalMap={closeModalMap} openModalOrder={openModalOrder}/>
            <ModalOrderRestaurantDetails {...props} map={modalMap}modalOrder={modalOrder} closeModalOrder={closeModalOrder}
                                         closeModalMap={closeModalMap} openModalChoose={openModalChoose} openModalRecap={openModalRecap} openModalMap={openModalMap}/>
            <ModalReacapChoice {...props} openModalOrder={openModalOrder} modalRecap={modalRecap}
                               closeModalRecap={closeModalRecap} closeModalOrder={closeModalOrder}
                               closeModalVisible={closeModalChoose} closeModalMap={closeModalMap}/>

            <ModalMap {...props}  modalMap={modalMap}closeModalMap={closeModalMap} openModalRecap={openModalRecap}/>
            <MenuBar {...props}/>
            <ScrollView  style={{display: 'flex', flex:1}}>
                <View style={{
                    backgroundColor: 'white',

                    justifyContent: 'center',
                    display: 'flex',
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                }}>
                    {props.point === null ?
                        <TouchableOpacity onPress={() => {
                            console.log('here start');
                            openModalChoose();
                        }}
                                          style={{
                                              alignItems: 'center',
                                              backgroundColor: '#E3002B',
                                              borderRadius: 50,
                                              padding: 16,
                                          }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >START MY ORDER</Text>
                        </TouchableOpacity>
                        :
                        <Place {...props} openModalChoose={openModalChoose}/>

                    }
                </View>
                <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{display: 'flex', justifyContent: 'flex-end'}}>
                    </View>
                    <View style={styles.threeTextContainer}>
                        <View style={styles.boxSimple}>
                        </View>
                        <View style={styles.boxSimple}>
                        </View>
                        <View style={styles.boxSimple}>
                        </View>
                    </View>
                </View>
                <ListDetails {...props}/>
            </ScrollView>
        </View>
    )
}
const mapStateToProps = state => {
    const {loading,indexed,orders,single,card,first,place,tot} = state.auth;
    const {check,menu,point}=state.appReducer;
    return {loading,check,menu,indexed,point,orders,single,card,first,place,tot};
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex:(index)=>dispatch(changeIndex(index)),
    savePoint: (col) => dispatch(savePoint(col)),
    calculateTotal:(card)=>dispatch(calculateTotal(card)),
    resetOrder: () => dispatch(resetOrder()),
});
export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen);
