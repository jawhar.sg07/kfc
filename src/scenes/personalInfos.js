// Imports: Dependencies
import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Button,Image, ScrollView, ActivityIndicator, Alert,TouchableWithoutFeedback,TouchableHighlight} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import Book from '../assets/images/book.svg'
import Location from '../assets/images/location.svg'
import {
    addToCard, calculateTotal,
    changeIndex,
    friteType,
    getMenu,
    resetOrder,
    sandwichType, sauceType,
    savePoint, SliceSauce,
    sodaType, submitPurchase,
} from '../actions/data';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import FormPersonal from '../components/formPersonal';
const radio_props = [
    {label: 'Mr', value: 0 },
    {label: 'Mrs', value: 1 }
];
const PersonalInfos = (props) => {
    const [value,setValue]=useState(1)
    useEffect(() => {

    }, []);

    return (


        <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
            <MenuBar {...props}/>
            <ScrollView style={{display: 'flex', flex: 1}}>
                <View style={styles.adventureView}>
                    <Text style={styles.adventureTitle}>INFORMATIONS PERSONNELLES</Text>

                </View>
                <View >
                    <Text style={{color:'#000000',fontSize:15,fontFamily:'Montserrat-Bold',marginHorizontal:30,marginBottom:20}}>Titre :</Text>
                    <RadioForm
                        style={{marginHorizontal:20,marginBottom:15}}
                        formHorizontal={true}
                        animation={true}
                    >
                        {/* To create radio buttons, loop through your array of options */}
                        {
                            radio_props.map((obj, i) => (
                                <RadioButton labelHorizontal={true} key={i} >
                                    {/*  You can set RadioButtonLabel before RadioButtonInput */}
                                    <RadioButtonInput
                                        obj={obj}
                                        index={i}

                                        isSelected={value=== i}
                                        onPress={(val)=>setValue(val)}
                                        borderWidth={1}
                                        buttonInnerColor={'#e74c3c'}
                                        buttonOuterColor={value === i ? '#E82322' : '#707070'}
                                        buttonSize={10}
                                        buttonOuterSize={23}
                                        buttonStyle={{}}
                                        buttonWrapStyle={{marginLeft: 10}}
                                    />
                                    <RadioButtonLabel
                                        obj={obj}
                                        index={i}
                                        labelHorizontal={true}
                                        onPress={(val)=>console.log('val',obj,i)}
                                        labelStyle={{fontSize: 12, color: '#000000',fontFamily:'Montserrat-Medium'}}
                                        labelWrapStyle={{}}
                                    />
                                </RadioButton>
                            ))
                        }
                    </RadioForm>
                    <FormPersonal {...props}/>
                </View>


            </ScrollView>

        </View>


    );
};
const mapStateToProps = state => {
    const {loading, indexed, tot, orders, clear, sandwich, place, soda, frite, sauce, sandwich_type, sauce_type, frite_type, soda_type, card} = state.auth;
    const {check, menu, point} = state.appReducer;

    return {
        place,
        loading,
        check,
        tot,
        menu,
        indexed,
        point,
        orders,
        clear,
        sandwich,
        soda,
        frite,
        sauce,
        sandwich_type,
        sauce_type,
        frite_type,
        soda_type,
        card,
    };
};

const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    addToCard: (data, navigation) => dispatch(addToCard(data, navigation)),

    sandwichType: (type) => dispatch(sandwichType(type)),
    sodaType: (type) => dispatch(sodaType(type)),
    friteType: (type) => dispatch(friteType(type)),
    sauceType: (type) => dispatch(sauceType(type)),
    savePoint: (col) => dispatch(savePoint(col)),
    SliceSauce: (type) => dispatch(SliceSauce(type)),
    calculateTotal: (card) => dispatch(calculateTotal(card)),
    resetOrder: () => dispatch(resetOrder()),
    submitPurchase: (qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot) => dispatch(submitPurchase(qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfos);
