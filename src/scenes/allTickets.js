// Imports: Dependencies
import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import Eye from '../assets/images/eye.svg'
import {
    calculateTotal,
    changeIndex, friteType,
    getCard,
    getFriteType,
    getMenu, getPlaces,
    getSandwichType, getSauceType,
    getSodaType,
    resetOrder, sandwichType, sauceType,
    savePoint, sodaType,
} from '../actions/data';
import moment from 'moment';
import Next from '../assets/images/next.svg';
import {goToSingle} from '../actions/action';
import SplashScreen from 'react-native-splash-screen'
import ModalMap from '../components/ModalMap';
const AllTickets = (props) => {
    console.log('allllllll',props);

    useEffect(() => {

    }, []);

    return (

        <View style={styles.homeContainer}>
            <MenuBar {...props}/>

            <ScrollView style={{display: 'flex', flex: 1,backgroundColor:'white'}}>
                <View style={styles.adventureView}>
                    <Text style={styles.adventureTitle}>HISTORIQUE DES COMMANDES</Text>

                </View>
                <View style={{marginHorizontal:30}}>
                    <Text style={{color:'#726F6F',fontFamily:'Montserrat-Medium',fontSize:14,lineHeight:20}}>Vous trouverez ici vos commandes passées depuis la création de votre compte</Text>
                </View>

                <FlatList
                    data={props.tickets.tickets}
                    renderItem={({ item,index }) => (


                            <View style={{
                                display: 'flex',
                                flexDirection: 'row',
                                flex: 1,
                                marginHorizontal: 10,
                                marginVertical: 15,




                                elevation: 1,
                                borderColor:'#00000029',

                                padding:20,
                                borderRadius:1


                            }}>
                                <View style={{flex:0.4}}>
                                    <Text style={{color:'#000000',fontFamily:'Montserrat-Bold',fontSize:15,marginBottom:10}}>{item.reference}</Text>

                                    <Text style={{color:'#717070',ontFamily:'Montserrat-medium',fontSize:12}}>{moment(item.date).format('L')} {moment(item.date).format('LT')}</Text>

                                    <Text style={{color:'#717070',ontFamily:'Montserrat-medium',fontSize:12}}>{Number(item?.total).toFixed(3)} DT</Text>
                                    <View style={{marginTop:15,borderColor:'#FF8C00',borderWidth:1,display:'flex',flexDirection:'row',justifyContent:'center',padding:10,borderRadius:4}}>
                                        <Text style={{color:'#FF8C00',fontSize:13,fontFamily:'Montserrat-Bold'}}>En cours</Text>
                                    </View>
                                </View>
                                <View style={{flex:0.6,flexDirection:'row',justifyContent:'flex-end'}}>
                                  <Eye/>

                                </View>


                            </View>



                    )}
                    //Setting the number of column

                    keyExtractor={(item, index) => item.reference.toString()}

                />

            </ScrollView>

        </View>
    );
};
const mapStateToProps = state => {
    const {loading, indexed,orders,single,card,first,place,tot} = state.auth;
    const {check, menu, point,tickets} = state.appReducer;

    return {loading, check, menu, indexed, point,orders,single,card,first,place,tot,tickets};
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    resetOrder: () => dispatch(resetOrder()),
    savePoint: (col) => dispatch(savePoint(col)),
    goToSingle:(one,navigation,old_index,item)=>dispatch(goToSingle(one,navigation,old_index,item)),
    getCard:()=>dispatch(getCard()),
    getSandwichType: () => dispatch(getSandwichType()),
    getSodaType: () => dispatch(getSodaType()),
    getFriteTypes: () => dispatch(getFriteType()),
    getSauceTypes: () => dispatch(getSauceType()),
    sandwichType: (type) => dispatch(sandwichType(type)),
    sodaType: (type) => dispatch(sodaType(type)),
    friteType: (type) => dispatch(friteType(type)),
    sauceType: (type) => dispatch(sauceType(type)),
    getPlaces:()=>dispatch(getPlaces()),
    calculateTotal:(card)=>dispatch(calculateTotal(card))
});

export default connect(mapStateToProps, mapDispatchToProps)(AllTickets);
