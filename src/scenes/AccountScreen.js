// Imports: Dependencies
import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Button,Image, ScrollView, ActivityIndicator, Alert,TouchableWithoutFeedback,TouchableHighlight} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import Book from '../assets/images/book.svg'
import Location from '../assets/images/location.svg'
import {
    addToCard, calculateTotal,
    changeIndex,
    friteType,
    getMenu,
    resetOrder,
    sandwichType, sauceType,
    savePoint, SliceSauce,
    sodaType, submitPurchase,
} from '../actions/data';
import Info from '../assets/images/info.svg'

const AccountScreen = (props) => {
    useEffect(() => {

    }, []);

    return (


        <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
            <MenuBar {...props}/>
            <ScrollView style={{display: 'flex', flex: 1}}>
                <View style={styles.adventureView}>
                    <Text style={styles.adventureTitle}>MON COMPTE</Text>

                </View>
                <TouchableOpacity onPress={()=>props.navigation.navigate('PersonalInfos')} style={styles.accountView}>
                    <Info/>
                    <Text style={{color:'black',fontSize:14,fontFamily:'Montserrat-Bold',marginLeft:20}}>INFORMATIONS</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.accountView}>
                    <Location/>
                    <Text style={{color:'black',fontSize:14,fontFamily:'Montserrat-Bold',marginLeft:20}}>ADRESSES</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.accountView}>
                    <Book/>
                    <Text style={{color:'black',fontSize:14,fontFamily:'Montserrat-Bold',marginLeft:20}}>HISTORIQUE DES COMMANDES</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Alert.alert('NOT YET !!!')}
                                  style={{
                                      alignItems: 'center',
                                      backgroundColor: 'white',
                                      borderRadius: 50,
                                      padding: 16,
                                      borderColor: 'black',
                                      borderWidth: 1,
                                      marginHorizontal:15,
                                      marginVertical:30
                                  }}

                >
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 14,
                            fontFamily: 'Montserrat-Bold',
                        }}

                    >DECONNEXION</Text>
                </TouchableOpacity>
            </ScrollView>

        </View>


    );
};
const mapStateToProps = state => {
    const {loading, indexed, tot, orders, clear, sandwich, place, soda, frite, sauce, sandwich_type, sauce_type, frite_type, soda_type, card} = state.auth;
    const {check, menu, point} = state.appReducer;

    return {
        place,
        loading,
        check,
        tot,
        menu,
        indexed,
        point,
        orders,
        clear,
        sandwich,
        soda,
        frite,
        sauce,
        sandwich_type,
        sauce_type,
        frite_type,
        soda_type,
        card,
    };
};

const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    addToCard: (data, navigation) => dispatch(addToCard(data, navigation)),

    sandwichType: (type) => dispatch(sandwichType(type)),
    sodaType: (type) => dispatch(sodaType(type)),
    friteType: (type) => dispatch(friteType(type)),
    sauceType: (type) => dispatch(sauceType(type)),
    savePoint: (col) => dispatch(savePoint(col)),
    SliceSauce: (type) => dispatch(SliceSauce(type)),
    calculateTotal: (card) => dispatch(calculateTotal(card)),
    resetOrder: () => dispatch(resetOrder()),
    submitPurchase: (qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot) => dispatch(submitPurchase(qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen);
