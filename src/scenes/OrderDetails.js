import React, { useEffect, useState } from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import {
    addToCard, calculateTotal,
    changeIndex, friteType,
    getFriteType,
    getMenu,
    getSandwichType,
    getSauceType,
    getSodaType, resetOrder, sandwichType, sauceType, savePoint, SliceSauce, sodaType, submitPurchase,
} from '../actions/data';
import Next from '../assets/images/next.svg';
import InputSpinner from 'react-native-input-spinner';
import More from '../assets/images/more.svg';
import TwoLists from '../components/TwoLists';
import ModalChooseOrder from '../components/ModalChooseOrder';
import ModalOrderRestaurantDetails from '../components/ModalOrderRestDetails';
import ModalReacapChoice from '../components/ModalRecapChoice';
import ModalMap from '../components/ModalMap';
import Place from '../components/place';


const OrderDetails = (props) => {


    const [quantity, setQuantity] = useState(1);
    const [modalVisible, setModalVisible] = useState(false);
    const [modalOrder, setModalOrder] = useState(false);
    const [modalRecap, setModalRecap] = useState(false);
    const [modalMap, setModalMap] = useState(false);
    const openModalMap=()=>{
        setModalMap(true)
    }
    const closeModalMap=()=>{
        setModalMap(false)
    }
    const openModalChoose = () => {
        setModalVisible(true);
    };
    const closeModalChoose = () => {
        setModalVisible(false);
    };
    const openModalOrder = () => {
        setModalOrder(true);
    };
    const closeModalOrder = () => {
        setModalOrder(false);
    };
    const openModalRecap = () => {
        setModalRecap(true);
    };
    const closeModalRecap = () => {
        setModalRecap(false);
    };

    const addObject = (item, navigation) => {

        props.submitPurchase(quantity,item,props.sandwich_type,props.soda_type,props.frite_type,props.sauce_type,navigation,props.tot)
    };
    useEffect(() => {

    }, []);

    return (
        <View style={styles.homeContainer}>
            <MenuBar {...props}/>
            <ModalChooseOrder {...props} modalVisible={modalVisible} closeModalVisible={closeModalChoose}
                              closeModalMap={closeModalMap} openModalOrder={openModalOrder}/>
            <ModalOrderRestaurantDetails {...props} map={modalMap}modalOrder={modalOrder} closeModalOrder={closeModalOrder}
                                         closeModalMap={closeModalMap} openModalChoose={openModalChoose} openModalRecap={openModalRecap} openModalMap={openModalMap}/>
            <ModalReacapChoice {...props} openModalOrder={openModalOrder} modalRecap={modalRecap}
                               closeModalRecap={closeModalRecap} closeModalOrder={closeModalOrder}
                               closeModalVisible={closeModalChoose} closeModalMap={closeModalMap}/>

            <ModalMap {...props}  modalMap={modalMap}closeModalMap={closeModalMap} openModalRecap={openModalRecap}/>


            <ScrollView style={{display: 'flex', flex: 1}}>

                <View style={{
                    backgroundColor: 'white',

                    justifyContent: 'center',
                    display: 'flex',
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                }}>
                    {props.point === null ?
                        <TouchableOpacity onPress={() => {
                            console.log('here start');
                            openModalChoose();
                        }}
                                          style={{
                                              alignItems: 'center',
                                              backgroundColor: '#E3002B',
                                              borderRadius: 50,
                                              padding: 16,
                                          }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >START MY ORDER</Text>
                        </TouchableOpacity>
                        :
                        <Place {...props} openModalChoose={openModalChoose}/>

                    }
                </View>
                <Image source={require('../assets/images/img30.png')} style={{width: wp('100%'), height: hp('25%')}}/>

                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginVertical: 20,
                    marginHorizontal: 20,
                }}>
                    <Text style={{
                        color: '#000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',

                    }}>{props.navigation.state.params.item.name}</Text>
                    <Text style={{
                        color: '#000',
                        fontSize: 14,
                        fontFamily: 'Montserrat-Black',

                    }}>{props.navigation.state.params.item.price} DT</Text>
                </View>
                <View style={{display: 'flex', justifyContent: 'center', alignItems: 'center', marginHorizontal: 20}}>
                    <Text style={{
                        color: '#434343',
                        fontSize: 14,
                        fontFamily: 'Montserrat-Regular',


                    }}>{props.navigation.state.params.item.description}</Text>

                </View>
                <View style={{display: 'flex', marginHorizontal: 20, marginVertical: 10}}>
                    <Text style={{
                        color: '#000',
                        fontSize: 14,
                        fontFamily: 'Montserrat-Bold',


                    }}>Allergens and nutrition information</Text>

                </View>

                <TwoLists {...props}/>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    padding: 25,
                    marginHorizontal: 15,
                    marginVertical: 20,
                    backgroundColor: 'white',
                }}>
                    <InputSpinner
                        max={10}
                        editable={false}
                        min={1}
                        step={1}
                        colorLeft={'#E3002B'}
                        colorRight={'#E3002B'}
                        showBorder={false}
                        color={'#E3002B'}
                        value="1"
                        onChange={(num) => {
                            setQuantity(Number(num));
                        }}
                    />
                    <Text style={{
                        color: '#000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',


                    }}>{(props.navigation.state.params.item.price * quantity).toFixed(3)} DT</Text>
                </View>
                <View style={{marginVertical: 15, marginHorizontal: 15}}>
                    <TouchableOpacity onPress={() => {
                        addObject(props.navigation.state.params.item, props.navigation);
                    }}
                                      style={{
                                          alignItems: 'center',
                                          backgroundColor: '#E3002B',
                                          borderRadius: 50,
                                          padding: 16,
                                      }}

                    >
                        <Text
                            style={{
                                color: 'white',
                                fontSize: 15,
                                fontFamily: 'Montserrat-Bold',
                            }}

                        >ADD TO ORDER</Text>
                    </TouchableOpacity>
                </View>
                <View style={{marginHorizontal: 15, marginVertical: 15}}>
                    <Text style={{
                        color: '#000000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',
                    }}> THIS GOES GREAT WITH...</Text>
                </View>
                <View>
                    <Text>ff{props.sauce_type.sauces.length}</Text>
                </View>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    padding: 20,
                    marginHorizontal: 15,
                    marginVertical: 10,
                    backgroundColor: 'white',
                }}>
                    <Image source={require('../assets/images/36.png')}/>
                    <View style={{marginRight: 15}}>
                        <Text style={{
                            color: '#000',
                            fontSize: 14,
                            fontFamily: 'Montserrat-Bold',


                        }}>
                            CREAM BALL
                        </Text>
                        <Text style={{
                            color: '#000',
                            fontSize: 16,
                            fontFamily: 'Montserrat-Black',


                        }}>+ 15.900 DT</Text>
                    </View>
                    <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            backgroundColor: '#E3002B',
                            borderRadius: 50,
                            padding: 16,
                            display: 'flex',
                            flexDirection: 'row',


                        }}

                    >
                        <More/>
                        <Text
                            style={{
                                color: 'white',
                                fontSize: 13,
                                fontFamily: 'Montserrat-Bold',
                                marginLeft: 5,
                            }}

                        >ADD </Text>
                    </TouchableOpacity>

                </View>
                <View style={{marginVertical: 20, marginHorizontal: 15}}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}
                                      style={{
                                          alignItems: 'center',
                                          backgroundColor: '#FEF4ED',
                                          borderRadius: 50,
                                          padding: 16,
                                          borderColor: 'black',
                                          borderWidth: 1,
                                      }}

                    >
                        <Text
                            style={{
                                color: 'black',
                                fontSize: 14,
                                fontFamily: 'Montserrat-Bold',
                            }}

                        >BACK TO BOX MEALS</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>


        </View>
    );
};
const mapStateToProps = state => {
    const {loading, indexed,tot, orders, clear, sandwich, place,soda, frite, sauce, sandwich_type, sauce_type, frite_type, soda_type,card} = state.auth;
    const {check, menu, point} = state.appReducer;

    return {
        place,
        loading,
        check,
        tot,
        menu,
        indexed,
        point,
        orders,
        clear,
        sandwich,
        soda,
        frite,
        sauce,
        sandwich_type,
        sauce_type,
        frite_type,
        soda_type,
        card
    };
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    addToCard: (data, navigation) => dispatch(addToCard(data, navigation)),

    sandwichType: (type) => dispatch(sandwichType(type)),
    sodaType: (type) => dispatch(sodaType(type)),
    friteType: (type) => dispatch(friteType(type)),
    sauceType: (type) => dispatch(sauceType(type)),
    savePoint: (col) => dispatch(savePoint(col)),
    SliceSauce: (type) => dispatch(SliceSauce(type)),
    calculateTotal:(card)=>dispatch(calculateTotal(card)),
    resetOrder: () => dispatch(resetOrder()),
    submitPurchase: (qty, name, sandwichType, sodaType, friteType, sauceType,navigation,ex_tot) => dispatch(submitPurchase(qty, name, sandwichType, sodaType, friteType, sauceType,navigation,ex_tot)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetails);
