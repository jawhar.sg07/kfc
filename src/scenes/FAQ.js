import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import Accordian from '../components/AccordonListFaq';
import {Colors} from '../config/color';
import styles from '../assets/styles';
import {
    addToCard, calculateTotal,
    changeIndex,
    friteType,
    getMenu, resetOrder,
    sandwichType,
    sauceType,
    savePoint, SliceSauce,
    sodaType, submitPurchase,
} from '../actions/data';
import {connect} from 'react-redux';
import MenuBar from '../components/MenuBar';

class FAQ extends Component {

    constructor(props) {
        super(props);
        this.state = {
            menu: [
                {
                    title: 'Quelle est la politique de KFC en matière d\'hygiène et de sécurité alimentaire dans les restaurants?',
                    data: 'KFC travaille avec ses propres experts et entreprises indépendantes. Chaque restaurant est soumis à des audits d\'hygiène et de sécurité alimentaire, à des contrôles sans parasites et à des audits d\'installations.',

                },
                {
                    title: 'Comment les contrôles d\'hygiène et de sécurité alimentaire sont-ils effectués dans les restaurants KFC?',
                    data: 'KFC travaille avec ses propres experts et entreprises indépendantes. Chaque restaurant est soumis à des audits d\'hygiène et de sécurité alimentaire, à des contrôles sans parasites et à des audits d\'installations.',

                },
                {
                    title: 'D\'où vient le poulet chez KFC?',
                    data: 'KFC travaille avec ses propres experts et entreprises indépendantes. Chaque restaurant est soumis à des audits d\'hygiène et de sécurité alimentaire, à des contrôles sans parasites et à des audits d\'installations.',

                },
                {
                    title: 'Quelle est la politique d\'emploi de KFC?',
                    data: 'KFC travaille avec ses propres experts et entreprises indépendantes. Chaque restaurant est soumis à des audits d\'hygiène et de sécurité alimentaire, à des contrôles sans parasites et à des audits d\'installations.',

                },
            ],
        };
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
                <MenuBar {...this.props}/>
                <ScrollView style={{display: 'flex',flex:1}}>
                    <View style={styles.adventureView}>

                        <Text style={styles.adventureTitle}>FAQ</Text>

                    </View>

                    {this.renderAccordians()}

                </ScrollView>
            </View>
        );
    }

    renderAccordians = () => {
        const items = [];
        for (item of this.state.menu) {
            items.push(
                <Accordian
                    title={item.title}
                    data={item.data}
                />,
            );
        }
        return items;
    };
}


const mapStateToProps = state => {
    const {loading, indexed, tot, orders, clear, sandwich, place, soda, frite, sauce, sandwich_type, sauce_type, frite_type, soda_type, card} = state.auth;
    const {check, menu, point} = state.appReducer;

    return {
        place,
        loading,
        check,
        tot,
        menu,
        indexed,
        point,
        orders,
        clear,
        sandwich,
        soda,
        frite,
        sauce,
        sandwich_type,
        sauce_type,
        frite_type,
        soda_type,
        card,
    };
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    addToCard: (data, navigation) => dispatch(addToCard(data, navigation)),

    sandwichType: (type) => dispatch(sandwichType(type)),
    sodaType: (type) => dispatch(sodaType(type)),
    friteType: (type) => dispatch(friteType(type)),
    sauceType: (type) => dispatch(sauceType(type)),
    savePoint: (col) => dispatch(savePoint(col)),
    SliceSauce: (type) => dispatch(SliceSauce(type)),
    calculateTotal: (card) => dispatch(calculateTotal(card)),
    resetOrder: () => dispatch(resetOrder()),
    submitPurchase: (qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot) => dispatch(submitPurchase(qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FAQ);
