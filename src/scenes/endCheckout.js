//screen 2 after choice meals from home
import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {
    addToCard, calculateTotal,
    changeIndex,
    friteType,
    getMenu, resetOrder,
    sandwichType,
    sauceType,
    savePoint, SliceSauce,
    sodaType, submitPurchase,
} from '../actions/data';
import Place from '../components/place';
import ModalChooseOrder from '../components/ModalChooseOrder';
import ModalOrderRestaurantDetails from '../components/ModalOrderRestDetails';
import ModalReacapChoice from '../components/ModalRecapChoice';
import ModalMap from '../components/ModalMap';
import Close from '../assets/images/close.svg';
import InputSpinner from 'react-native-input-spinner';
import styles from '../assets/styles';
import {backHome} from '../actions/action';


const EndCheckout = (props) => {


    return (
        <ScrollView style={{display: 'flex', flex: 1, backgroundColor: 'white'}}>


            <View style={{
                backgroundColor: 'white',

                justifyContent: 'center',
                display: 'flex',
                paddingHorizontal: 10,
                paddingVertical: 10,
            }}>
                <Text>Livraison Restaurant</Text>
                <Text>{props.point?.label}</Text>
            </View>
            <View style={{backgroundColor: '#FEF4ED', padding: 15}}>
                <View>
                    <Text style={{color: 'black', fontFamily: 'Montserrat-Black', fontSize: 18}}>SEE YOU IN STORE AT
                        7PM</Text>
                    <Text style={{
                        color: 'black',
                        fontFamily: 'Montserrat-Black',
                        fontSize: 18,
                    }}>{props.profile?.lastName}</Text>
                </View>
                <View style={{marginVertical: 15}}>
                    <Text style={{color: '#474545', fontSize: 14, fontFamily: 'Montserrat-Medium'}}>A confirmation
                        message has been sent on your phone number {props.profile.phoneNumber}</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    display: 'flex',
                    flexDirection: 'column',
                }}>
                    <Text style={{color: 'black', fontFamily: 'Montserrat-Black', fontSize: 15, marginVertical: 10}}>ORDER
                        Reference :</Text>
                    <Text style={{
                        color: 'black',
                        fontFamily: 'Montserrat-Black',
                        fontSize: 15,
                        marginVertical: 10,
                    }}>{props.ticket.reference}</Text>
                    <Image source={require('../assets/images/ref.png')}/>


                </View>
                <Text style={{fontFamily: 'Montserrat-ExtraBold', fontSize: 14, marginVertical: 15}}>Store you're
                    collecting from :</Text>
                <Text style={{
                    fontFamily: 'Montserrat-ExtraBold',
                    fontSize: 14,
                    marginVertical: 5,
                }}>{props.point?.label}</Text>
                <Text style={{
                    color: '#6B6969',
                    fontFamily: 'Montserrat-Regular',
                    fontSize: 14,
                    marginBottom: 10,
                }}>{props.point?.description}</Text>


            </View>
            <Text style={{
                color: 'black',
                fontFamily: 'Montserrat-Black',
                fontSize: 18,
                marginVertical: 15,
                marginHorizontal: 10,
            }}>You've ordered :</Text>
            <FlatList

                data={props.ticket.data}
                renderItem={({item, index}) => (

                    <View style={{
                        backgroundColor: 'white',
                        marginBottom: 20,
                        padding: 20,
                        borderWidth: 0.5,
                        borderColor: '#949494',
                        marginHorizontal: 15,
                    }}>
                        <View style={{
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginBottom: 10,
                        }}>
                            <View style={{display: 'flex', flexDirection: 'row'}}>
                                <Text style={{
                                    color: '#000000',
                                    fontSize: 14,
                                    fontFamily: 'Montserrat-Black',
                                }}>{item[0].quantity}x </Text>
                                <Text
                                    style={{
                                        color: '#000000',
                                        fontSize: 14,
                                        fontFamily: 'Montserrat-Black',
                                    }}>{item[0].product.name}</Text>

                            </View>


                        </View>
                        <View style={{
                            display: 'flex',
                            flexDirection: 'row',
                            marginHorizontal: 5,
                            marginVertical: 5,
                        }}>
                            <Text style={{
                                color: '#949494',
                                fontSize: 12,
                                fontFamily: 'Montserrat-Bold',
                            }}> Type {item[0].type.label} x1</Text>

                        </View>
                        {item[0].soda.label &&
                        <View style={{
                            display: 'flex',
                            flexDirection: 'row',
                            marginHorizontal: 5,
                            marginVertical: 5,
                        }}>
                            <Text style={{
                                color: '#949494',
                                fontSize: 12,
                                fontFamily: 'Montserrat-Bold',
                            }}> Soda {item[0].soda.label} x1</Text>

                        </View>}
                        {item[0].frite.label &&
                        <View style={{
                            display: 'flex',
                            flexDirection: 'row',
                            marginHorizontal: 5,
                            marginVertical: 5,
                        }}>
                            <Text style={{
                                color: '#949494',
                                fontSize: 12,
                                fontFamily: 'Montserrat-Bold',
                            }}>Frite {item[0].frite.label} x1</Text>

                        </View>}
                        {item[0].sauce.sauces.length !== 0 &&
                        <View style={{
                            display: 'flex',
                            flexDirection: 'column',
                            marginHorizontal: 5,
                            marginVertical: 5,
                        }}>
                            <Text style={{
                                color: '#949494',
                                fontSize: 12,
                                fontFamily: 'Montserrat-Bold',
                            }}>Dipping Sauce </Text>
                            {item[0]?.sauce.sauces.map((item) => {
                                return <Text>{item.label}</Text>;
                            })}

                        </View>}


                        <View style={{
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginTop: 10,

                        }}>

                            <Text style={{
                                color: '#000',
                                fontSize: 16,
                                fontFamily: 'Montserrat-Black',


                            }}>{(((Number(item[0].product?.price)) +
                                (item[0].sauce.sauces[0] ? item[0].sauce.sauces[0].price : 0) +
                                (item[0].frite.label === 'Large' ? (item[0].frite.price) : 0)) * item[0].quantity).toFixed(3)} DT</Text>
                        </View>

                    </View>

                )}


                //Setting the number of column

                keyExtractor={(item, index) => index.toString()}
            />
            <View style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
            }}>
                <View style={{marginHorizontal: 20, marginVertical: 25}}>
                    <Text style={styles.textCheck}>
                        Sub total
                    </Text>
                    <Text style={styles.textCheck}>
                        Delivery
                    </Text>
                    <Text style={styles.textCheck}>
                        Tax
                    </Text>
                    <Text style={{
                        color: '#000000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',
                        marginBottom: 15,
                    }}>
                        TOTAL
                    </Text>
                </View>
                <View style={{marginHorizontal: 20, marginVertical: 25}}>
                    <Text style={styles.textCheckRight}>
                        {props.tot.toFixed(3)}
                    </Text>
                    <Text style={styles.textCheckRight}>
                        FREE
                    </Text>
                    <Text style={styles.textCheckRight}>
                        1.000 DT
                    </Text>
                    <Text style={{
                        color: '#000000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',
                        marginBottom: 15,
                    }}>
                        {(props.tot + 1).toFixed(3)}
                    </Text>
                </View>

            </View>

            <TouchableOpacity onPress={() => {
                props.backHome(props.navigation);

            }} style={{
                alignItems: 'center',
                backgroundColor: 'white',
                borderRadius: 50,
                padding: 16,
                borderColor: 'black',
                borderWidth: 1,
                marginHorizontal: 10,
                marginBottom: 15,
            }}

            >
                <Text
                    style={{
                        color: 'black',
                        fontSize: 15,
                        fontFamily: 'Montserrat-Bold',

                    }}

                >BACK TO HOME</Text>
            </TouchableOpacity>
        </ScrollView>

    );
};
const mapStateToProps = state => {
    const {ticket, place, tot} = state.auth;
    const {tickets, point, profile} = state.appReducer;

    return {
        tickets, ticket, point, place, profile, tot,
    };
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    addToCard: (data, navigation) => dispatch(addToCard(data, navigation)),
    backHome: (navigation) => dispatch(backHome(navigation)),
    sandwichType: (type) => dispatch(sandwichType(type)),
    sodaType: (type) => dispatch(sodaType(type)),
    friteType: (type) => dispatch(friteType(type)),
    sauceType: (type) => dispatch(sauceType(type)),
    savePoint: (col) => dispatch(savePoint(col)),
    SliceSauce: (type) => dispatch(SliceSauce(type)),
    calculateTotal: (card) => dispatch(calculateTotal(card)),
    resetOrder: () => dispatch(resetOrder()),
    submitPurchase: (qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot) => dispatch(submitPurchase(qty, name, sandwichType, sodaType, friteType, sauceType, navigation, ex_tot)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EndCheckout);
