import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import {calculateTotal, changeIndex, changeQuantity, deleteOrder, getMenu, resetOrder} from '../actions/data';
import ModalChooseOrder from '../components/ModalChooseOrder';
import ModalOrderRestaurantDetails from '../components/ModalOrderRestDetails';
import ModalReacapChoice from '../components/ModalRecapChoice';
import ListMeals from '../components/ListMealsForMe';
import Next from '../assets/images/next.svg';
import ListDetails from '../components/ListDetails';
import TwoLists from '../components/TwoLists';
import InputSpinner from 'react-native-input-spinner';
import More from '../assets/images/more.svg';
import Close from '../assets/images/close.svg';

const CheckoutScreen = (props) => {
    const FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: '100%',
                    backgroundColor: '#000',
                }}
            />
        );
    };
    return (
        <View style={styles.homeContainer}>
            <MenuBar {...props}/>

            <ScrollView style={{display: 'flex', flex: 1}}>

                <View style={{
                    backgroundColor: 'white',
                    paddingVertical: 20,
                    display: 'flex',
                    flexDirection: 'row',
                    paddingHorizontal: 10,
                    alignItems: 'center',
                }}>
                    <View style={{
                        backgroundColor: 'white',

                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                        <View style={{
                            display: 'flex',
                            flexDirection: 'column',
                            flex: 0.95,
                            justifyContent: 'center',
                        }}>
                            <Text style={{
                                color: '#474545',
                                fontSize: 12,

                                fontFamily: 'Montserrat-Bold',
                            }}>Carryout from:</Text>
                            <Text style={{
                                color: '#000000',
                                fontSize: 14,
                                fontFamily: 'Montserrat-Bold',
                            }}>{props.point?.name} at 7 pm</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() => {

                            }} style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',

                                backgroundColor: 'white',
                            }}

                            >
                                <Text
                                    style={{
                                        color: '#E3002B',
                                        fontSize: 15,
                                        fontFamily: 'Montserrat-ExtraBold',
                                        marginRight: 5,

                                    }}

                                >change</Text>
                                <Next/>
                            </TouchableOpacity>

                        </View>

                    </View>

                </View>

                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginVertical: 35,

                }}>
                    <Text style={{
                        color: '#000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',

                    }}>YOUR ORDER</Text>

                </View>
                <View style={{
                    display: 'flex',
                    marginHorizontal: 20,
                    backgroundColor: '#FEF4ED',

                }}>
                    <FlatList

                        data={props.card.cardData}
                        renderItem={({item, index}) => (

                            <View style={{backgroundColor: 'white', marginBottom: 20, padding: 20}}>
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 10,
                                }}>
                                    <View style={{display: 'flex', flexDirection: 'row'}}>
                                        <Text style={{
                                            color: '#E3002B',
                                            fontSize: 14,
                                            fontFamily: 'Montserrat-Bold',
                                        }}>{item[0].quantity}x </Text>
                                        <Text
                                            style={{
                                                color: '#E3002B',
                                                fontSize: 15,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>{item[0].product.name}</Text>

                                    </View>
                                    <TouchableOpacity style={{padding:10}} onPress={()=>{
                                        props.deleteOrder(index,item[0], props.card)
                                    }} >

                                    <Close/>
                                    </TouchableOpacity>

                                </View>
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    marginHorizontal: 5,
                                    marginVertical: 5,
                                }}>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 12,
                                        fontFamily: 'Montserrat-Bold',
                                    }}> Type {item[0].type.label} x1</Text>

                                </View>
                                {item[0].soda.label &&
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    marginHorizontal: 5,
                                    marginVertical: 5,
                                }}>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 12,
                                        fontFamily: 'Montserrat-Bold',
                                    }}> Soda {item[0].soda.label} x1</Text>

                                </View>}
                                {item[0].frite.label &&
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    marginHorizontal: 5,
                                    marginVertical: 5,
                                }}>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 12,
                                        fontFamily: 'Montserrat-Bold',
                                    }}>Frite {item[0].frite.label} x1</Text>

                                </View>}
                                {item[0].sauce.sauces.length !== 0 &&
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    marginHorizontal: 5,
                                    marginVertical: 5,
                                }}>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 12,
                                        fontFamily: 'Montserrat-Bold',
                                    }}>Dipping Sauce </Text>
                                    {item[0]?.sauce.sauces.map((item) => {
                                        return <Text>{item.label}</Text>;
                                    })}

                                </View>}


                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 10,

                                }}>
                                    <InputSpinner
                                        max={10}
                                        editable={false}
                                        min={1}
                                        step={1}
                                        colorLeft={'#E3002B'}
                                        colorRight={'#E3002B'}
                                        showBorder={false}
                                        color={'#E3002B'}
                                        value={item[0].quantity.toString()}
                                        onChange={(num) => {
                                            props.changeQuantity(index, item[0], num, props.card);
                                        }}
                                    />
                                    <Text style={{
                                        color: '#000',
                                        fontSize: 16,
                                        fontFamily: 'Montserrat-Black',


                                    }}>{(( (Number(item[0].product?.price))+
                                        (item[0].sauce.sauces[0]?item[0].sauce.sauces[0].price:0) +
                                        (item[0].frite.label==='Large'?(item[0].frite.price):0))*item[0].quantity).toFixed(3)} DT</Text>
                                </View>
                            </View>

                        )}


                        //Setting the number of column

                        keyExtractor={(item, index) => index.toString()}
                    />


                </View>

                <View style={{marginHorizontal: 15, marginVertical: 15}}>
                    <Text style={{
                        color: '#000000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',
                    }}> THIS GOES GREAT WITH...</Text>
                </View>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    padding: 20,
                    marginHorizontal: 15,
                    marginVertical: 10,
                    backgroundColor: 'white',
                }}>
                    <Image source={require('../assets/images/36.png')}/>
                    <View style={{marginRight: 15}}>
                        <Text style={{
                            color: '#000',
                            fontSize: 14,
                            fontFamily: 'Montserrat-Bold',


                        }}>
                            CREAM BALL
                        </Text>
                        <Text style={{
                            color: '#000',
                            fontSize: 16,
                            fontFamily: 'Montserrat-Black',


                        }}>+ 15.900 DT</Text>
                    </View>

                    <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            backgroundColor: '#E3002B',
                            borderRadius: 50,
                            padding: 16,
                            display: 'flex',
                            flexDirection: 'row',


                        }}

                    >
                        <More/>
                        <Text
                            style={{
                                color: 'white',
                                fontSize: 13,
                                fontFamily: 'Montserrat-Bold',
                                marginLeft: 5,
                            }}

                        >ADD </Text>
                    </TouchableOpacity>

                </View>
                <View style={{marginVertical: 20, marginHorizontal: 15}}>

                    <View style={{marginVertical: 15, marginHorizontal: 5}}>

                        <TouchableOpacity
                            style={{
                                alignItems: 'center',
                                backgroundColor: '#FEF4ED',
                                borderRadius: 50,
                                padding: 16,
                                borderColor: 'black',
                                borderWidth: 1,
                            }}

                        >
                            <Text
                                style={{
                                    color: 'black',
                                    fontSize: 14,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >CONTINUE ORDERING</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <View style={{backgroundColor: 'white'}}>
                    <View style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginVertical: 15,
                        marginHorizontal: 15,
                        alignItems: 'center',
                    }}>
                        <Text style={{
                            color: 'black',
                            fontSize: 15,
                            fontFamily: 'Montserrat-Medium',
                        }}>Enter a coucher code</Text>
                        <TouchableOpacity
                            style={{
                                alignItems: 'center',
                                backgroundColor: 'white',
                                borderRadius: 50,
                                paddingHorizontal: 20,
                                paddingVertical: 15,
                                borderColor: 'black',
                                borderWidth: 1,
                            }}

                        >
                            <Text
                                style={{
                                    color: 'black',
                                    fontSize: 14,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >APPLY</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                        <View style={{marginHorizontal: 20, marginVertical: 25}}>
                            <Text style={styles.textCheck}>
                                Sub total
                            </Text>
                            <Text style={styles.textCheck}>
                                Delivery
                            </Text>
                            <Text style={styles.textCheck}>
                                Tax
                            </Text>
                            <Text style={{
                                color: '#000000',
                                fontSize: 16,
                                fontFamily: 'Montserrat-Black',
                                marginBottom: 15,
                            }}>
                                TOTAL
                            </Text>
                        </View>
                        <View style={{marginHorizontal: 20, marginVertical: 25}}>
                            <Text style={styles.textCheckRight}>
                                {props.tot.toFixed(3)}
                            </Text>
                            <Text style={styles.textCheckRight}>
                                FREE
                            </Text>
                            <Text style={styles.textCheckRight}>
                                1.000 DT
                            </Text>
                            <Text style={{
                                color: '#000000',
                                fontSize: 16,
                                fontFamily: 'Montserrat-Black',
                                marginBottom: 15,
                            }}>
                                {(props.tot+1).toFixed(3)}
                            </Text>
                        </View>

                    </View>
                    <View style={{marginVertical: 15, marginHorizontal: 20}}>
                        <TouchableOpacity onPress={() => {
                            props.navigation.navigate('RecapCheckout');
                        }}
                                          style={{
                                              alignItems: 'center',
                                              backgroundColor: '#E3002B',
                                              borderRadius: 50,
                                              padding: 16,

                                              marginBottom: 15,
                                          }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 14,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >PROCEED TO PAYMENT</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>

        </View>

    );

};
const mapStateToProps = state => {
    const {loading, indexed, orders, card, tot} = state.auth;
    const {check, menu, point} = state.appReducer;

    return {loading, check, menu, indexed, point, orders, card, tot};
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    calculateTotal: (card) => dispatch(calculateTotal(card)),
    changeQuantity: (index, item, qte, all) => dispatch(changeQuantity(index, item, qte, all)),
    deleteOrder:(index,item,all)=>dispatch(deleteOrder(index,item,all)),
    resetOrder: () => dispatch(resetOrder()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutScreen);
