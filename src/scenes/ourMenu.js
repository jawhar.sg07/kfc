// Imports: Dependencies
import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import ListMeals from '../components/ListMealsForMe';
import ModalChooseOrder from '../components/ModalChooseOrder';
import ModalOrderRestaurantDetails from '../components/ModalOrderRestDetails';
import ModalReacapChoice from '../components/ModalRecapChoice';
import Place from '../components/place';
import {
    calculateTotal,
    changeIndex, friteType,
    getCard,
    getFriteType,
    getMenu, getPlaces,
    getSandwichType, getSauceType,
    getSodaType,
    resetOrder, sandwichType, sauceType,
    savePoint, sodaType,
} from '../actions/data';
import Next from '../assets/images/next.svg';
import {goToSingle} from '../actions/action';
import SplashScreen from 'react-native-splash-screen'
import ModalMap from '../components/ModalMap';
const OurMenu = (props) => {

    const [modalVisible, setModalVisible] = useState(false);
    const [modalOrder, setModalOrder] = useState(false);
    const [modalRecap, setModalRecap] = useState(false);
    const [modalMap, setModalMap] = useState(false);
    const openModalMap=()=>{
        setModalMap(true)
    }
    const closeModalMap=()=>{
        setModalMap(false)
    }
    const openModalChoose = () => {
        setModalVisible(true);
    };
    const closeModalChoose = () => {
        setModalVisible(false);
    };
    const openModalOrder = () => {
        setModalOrder(true);
    };
    const closeModalOrder = () => {
        setModalOrder(false);
    };
    const openModalRecap = () => {
        setModalRecap(true);
    };
    const closeModalRecap = () => {
        setModalRecap(false);
    };

    useEffect(() => {
        console.log('props home', props);
        props.getMenu();
        props.getCard();
        props.getPlaces();
        props.getSandwichType();
        props.getSodaType();
        props.getFriteTypes();
        props.getSauceTypes();
        SplashScreen.hide();
    }, []);

    return (

        <View style={styles.homeContainer}>
            <MenuBar {...props}/>
            <ModalChooseOrder {...props} modalVisible={modalVisible} closeModalVisible={closeModalChoose}
                              closeModalMap={closeModalMap} openModalOrder={openModalOrder}/>
            <ModalOrderRestaurantDetails {...props} map={modalMap}modalOrder={modalOrder} closeModalOrder={closeModalOrder}
                                         closeModalMap={closeModalMap} openModalChoose={openModalChoose} openModalRecap={openModalRecap} openModalMap={openModalMap}/>
            <ModalReacapChoice {...props} openModalOrder={openModalOrder} modalRecap={modalRecap}
                               closeModalRecap={closeModalRecap} closeModalOrder={closeModalOrder}
                               closeModalVisible={closeModalChoose} closeModalMap={closeModalMap}/>
            {modalMap &&

            <ModalMap {...props} modalMap={modalMap} closeModalMap={closeModalMap} openModalRecap={openModalRecap}/>

            }
            <ScrollView style={{display: 'flex', flex: 1}}>
                <View style={{
                    backgroundColor: 'white',

                    justifyContent: 'center',
                    display: 'flex',
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                }}>
                    {props.point === null ?
                        <TouchableOpacity onPress={() => {
                            console.log('here start');
                            openModalChoose();
                        }}
                                          style={{
                                              alignItems: 'center',
                                              backgroundColor: '#E3002B',
                                              borderRadius: 50,
                                              padding: 16,
                                          }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >START MY ORDER</Text>
                        </TouchableOpacity>
                        :
                       <Place {...props} openModalChoose={openModalChoose}/>

                    }
                </View>
                <Image source={require('../assets/images/img1.png')} style={{width: wp('100%'), height: hp('30%')}}/>
                <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>

                    <View style={{display: 'flex', justifyContent: 'flex-end'}}>
                        <Text style={{
                            color: 'black',
                            fontSize: 18,
                            paddingHorizontal: 15,
                            fontFamily: 'Montserrat-Black',
                        }}>NOTRE CARTE</Text>
                    </View>
                    <View style={styles.threeTextContainer}>
                        <View style={styles.boxSimple}>
                        </View>
                        <View style={styles.boxSimple}>
                        </View>
                        <View style={styles.boxSimple}>
                        </View>

                    </View>
                </View>

                <View>
                    <ListMeals {...props}/>
                </View>

            </ScrollView>

        </View>
    );
};
const mapStateToProps = state => {
    const {loading, indexed,orders,single,card,first,place,tot} = state.auth;
    const {check, menu, point} = state.appReducer;

    return {loading, check, menu, indexed, point,orders,single,card,first,place,tot};
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    resetOrder: () => dispatch(resetOrder()),
    savePoint: (col) => dispatch(savePoint(col)),
    goToSingle:(one,navigation,old_index,item)=>dispatch(goToSingle(one,navigation,old_index,item)),
    getCard:()=>dispatch(getCard()),
getSandwichType: () => dispatch(getSandwichType()),
    getSodaType: () => dispatch(getSodaType()),
    getFriteTypes: () => dispatch(getFriteType()),
    getSauceTypes: () => dispatch(getSauceType()),
    sandwichType: (type) => dispatch(sandwichType(type)),
    sodaType: (type) => dispatch(sodaType(type)),
    friteType: (type) => dispatch(friteType(type)),
    sauceType: (type) => dispatch(sauceType(type)),
    getPlaces:()=>dispatch(getPlaces()),
    calculateTotal:(card)=>dispatch(calculateTotal(card))
});

export default connect(mapStateToProps, mapDispatchToProps)(OurMenu);
