import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import {calculateTotal, changeIndex, getMenu, resetOrder} from '../actions/data';
import ModalChooseOrder from '../components/ModalChooseOrder';
import ModalOrderRestaurantDetails from '../components/ModalOrderRestDetails';
import ModalReacapChoice from '../components/ModalRecapChoice';
import ListMeals from '../components/ListMealsForMe';
import Next from '../assets/images/next.svg';
import ListDetails from '../components/ListDetails';
import TwoLists from '../components/TwoLists';
import InputSpinner from 'react-native-input-spinner';
import More from '../assets/images/more.svg';

const RecapOrder = (props) => {
    console.log('props recap order', props);
    const list = [
        {
            name: 'ZINGER BOX MEAL',
            qty: 1,
            price: 15.900,
            items: [
                {
                    name: 'zinger Burger',
                    qty: 1,
                },
                {
                    name: 'white meat with wings',
                    qty: 2,
                },
                {
                    name: 'rehular fries',
                    qty: 1,
                },
                {
                    name: 'regular pepsi max',
                    qty: 1,
                },
            ],

        },


    ];
    return (
        <View style={styles.homeContainer}>
            <MenuBar {...props}/>

            <ScrollView style={{display: 'flex', flex: 1}}>

                <View style={{
                    backgroundColor: 'white',
                    paddingVertical: 20,
                    display: 'flex',
                    flexDirection: 'row',
                    paddingHorizontal: 10,
                    alignItems: 'center',
                }}>
                    <View style={{
                        backgroundColor: 'white',

                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                        <View style={{
                            display: 'flex',
                            flexDirection: 'column',
                            flex: 0.95,
                            justifyContent: 'center',
                        }}>
                            <Text style={{
                                color: '#474545',
                                fontSize: 12,

                                fontFamily: 'Montserrat-Bold',
                            }}>Carryout from:</Text>
                            <Text style={{
                                color: '#000000',
                                fontSize: 14,
                                fontFamily: 'Montserrat-Bold',
                            }}>{props.point?.name} at 7 pm</Text>
                        </View>
                        <View>
                            <TouchableOpacity onPress={() => {

                            }} style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',

                                backgroundColor: 'white',
                            }}

                            >
                                <Text
                                    style={{
                                        color: '#E3002B',
                                        fontSize: 15,
                                        fontFamily: 'Montserrat-ExtraBold',
                                        marginRight:5

                                    }}

                                >change</Text>
                                <Next/>
                            </TouchableOpacity>

                        </View>

                    </View>
                </View>
                <Image source={require('../assets/images/img30.png')} style={{width: wp('100%'), height: hp('25%')}}/>

                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginVertical: 20,
                    marginHorizontal: 20,
                }}>
                    <Text style={{
                        color: '#000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',

                    }}>YOU' VE ADDED</Text>

                </View>
                <View style={{display: 'flex', marginHorizontal: 20}}>
                    <FlatList

                        data={props.card.cardData}
                        renderItem={({item}) => (

                            <View>
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 10,
                                }}>
                                    <View style={{display: 'flex', flexDirection: 'row'}}>
                                        <Text style={{
                                            color: '#E3002B',
                                            fontSize: 14,
                                            fontFamily: 'Montserrat-Bold',
                                        }}>{item[0].quantity} x </Text>
                                        <Text
                                            style={{
                                                color: '#E3002B',
                                                fontSize: 15,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>{item[0].product.name}</Text>

                                    </View>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 15,
                                        fontFamily: 'Montserrat-Black',
                                    }}>{item[0].product?.price} DT</Text>
                                </View>
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 10,
                                }}>
                                    <View style={{display: 'flex', flexDirection: 'row'}}>
                                        <View>
                                        <Text style={{
                                            color: 'black',
                                            fontSize: 14,
                                            fontFamily: 'Montserrat-Bold',
                                        }}>Type Sandwich </Text>
                                        <Text>
                                            {item[0].type.label} x1
                                        </Text>
                                        </View>


                                    </View>

                                </View>
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 10,
                                }}>
                                    {item[0].soda.label &&
                                    <View style={{display: 'flex', flexDirection: 'row'}}>
                                        <View>
                                            <Text style={{
                                                color: 'black',
                                                fontSize: 14,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>Soda</Text>
                                            <Text>
                                                {item[0].soda.label} x1
                                            </Text>
                                        </View>


                                    </View>
                                    }

                                </View>
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 10,
                                }}>
                                    {item[0].frite.label &&
                                    <View style={{display: 'flex', flexDirection: 'row'}}>
                                        <View>
                                            <Text style={{
                                                color: 'black',
                                                fontSize: 14,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>Frite</Text>
                                            <Text>
                                                {item[0].frite.label} {item[0].frite.price ? 'X' : ''} {item[0].frite?.price?.toFixed(3)} {item[0].frite.price ? 'DT' : ''}
                                            </Text>
                                        </View>


                                    </View>
                                    }

                                </View>
                                {item[0].sauce.sauces.length !==0 &&
                                <View style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 10,
                                }}>
                                    <View style={{display: 'flex', flexDirection: 'row'}}>
                                        <View>
                                            <Text style={{
                                                color: 'black',
                                                fontSize: 14,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>Dipping Sauce </Text>
                                            {item[0]?.sauce.sauces.map((item)=>{
                                                return <Text>{item.label}</Text>
                                            })}
                                        </View>


                                    </View>
                                    <Text style={{
                                        color: 'black',
                                        fontSize: 15,
                                        fontFamily: 'Montserrat-Black',
                                    }}>{props.sauce.price.toFixed(3)} DT</Text>
                                </View>
}

                                {/*<FlatList

                                    data={item.items}
                                    renderItem={({item}) => (

                                        <View style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            marginHorizontal: 5,
                                            marginVertical: 5,
                                        }}>
                                            <Text style={{
                                                color: 'black',
                                                fontSize: 12,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>{item.qty} x </Text>
                                            <Text style={{
                                                color: 'black',
                                                fontSize: 12,
                                                fontFamily: 'Montserrat-Bold',
                                            }}>{item.name}</Text>
                                        </View>

                                    )}
                                    //Setting the number of column

                                    keyExtractor={(item, index) => index.toString()}
                                />*/}
                            </View>

                        )}
                        //Setting the number of column

                        keyExtractor={(item, index) => index.toString()}
                    />


                </View>

                <View style={{marginHorizontal: 15, marginVertical: 15}}>
                    <Text style={{
                        color: '#000000',
                        fontSize: 16,
                        fontFamily: 'Montserrat-Black',
                    }}> THIS GOES GREAT WITH...</Text>
                </View>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    padding: 20,
                    marginHorizontal: 15,
                    marginVertical: 10,
                    backgroundColor: 'white',
                }}>
                    <Image source={require('../assets/images/36.png')}/>
                    <View style={{marginRight: 15}}>
                        <Text style={{
                            color: '#000',
                            fontSize: 14,
                            fontFamily: 'Montserrat-Bold',


                        }}>
                            CREAM BALL
                        </Text>
                        <Text style={{
                            color: '#000',
                            fontSize: 16,
                            fontFamily: 'Montserrat-Black',


                        }}>+ 15.900 DT</Text>
                    </View>

                    <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            backgroundColor: '#E3002B',
                            borderRadius: 50,
                            padding: 16,
                            display: 'flex',
                            flexDirection: 'row',


                        }}

                    >
                        <More/>
                        <Text
                            style={{
                                color: 'white',
                                fontSize: 13,
                                fontFamily: 'Montserrat-Bold',
                                marginLeft: 5,
                            }}

                        >ADD </Text>
                    </TouchableOpacity>

                </View>
                <View style={{marginVertical: 20, marginHorizontal: 15}}>

                    <View style={{marginVertical: 15, marginHorizontal: 5}}>
                        <TouchableOpacity onPress={()=>{
                            props.navigation.navigate('CheckoutScreen')
                        }}
                            style={{
                                alignItems: 'center',
                                backgroundColor: '#E3002B',
                                borderRadius: 50,
                                padding: 16,

                                marginBottom:15
                            }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 14,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >CHECKOUT</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={()=>{
                                props.navigation.navigate('DetailsScreen')
                            }}
                            style={{
                                alignItems: 'center',
                                backgroundColor: '#FEF4ED',
                                borderRadius: 50,
                                padding: 16,
                                borderColor: 'black',
                                borderWidth: 1,
                            }}

                        >
                            <Text
                                style={{
                                    color: 'black',
                                    fontSize: 14,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >BACK TO BOX MEALS</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </ScrollView>

        </View>
    );

};
const mapStateToProps = state => {
    const {loading, indexed,orders,card,sauce,tot} = state.auth;
    const {check, menu,point} = state.appReducer;

    return {loading, check, menu, indexed,point,orders,card,sauce,tot};
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    calculateTotal:(card)=>dispatch(calculateTotal(card)),
    resetOrder: () => dispatch(resetOrder()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RecapOrder);
