// Imports: Dependencies
import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import ListMeals from '../components/ListMealsForMe';
import ModalChooseOrder from '../components/ModalChooseOrder';
import ModalOrderRestaurantDetails from '../components/ModalOrderRestDetails';
import ModalReacapChoice from '../components/ModalRecapChoice';
import {calculateTotal, changeIndex, getMenu, getOne, resetOrder, savePoint} from '../actions/data';
import Next from '../assets/images/next.svg';
import {goToSingle} from '../actions/action';
import SplashScreen from 'react-native-splash-screen'
import SubList from '../components/subList'
const Home = (props) => {
    console.log('allll props homeeee', props);
    const [modalVisible, setModalVisible] = useState(false);
    const [modalOrder, setModalOrder] = useState(false);
    const [modalRecap, setModalRecap] = useState(false);
    const openModalChoose = () => {
        setModalVisible(true);
    };
    const closeModalChoose = () => {
        setModalVisible(false);
    };
    const openModalOrder = () => {
        setModalOrder(true);
    };
    const closeModalOrder = () => {
        setModalOrder(false);
    };
    const openModalRecap = () => {
        setModalRecap(true);
    };
    const closeModalRecap = () => {
        setModalRecap(false);
    };

    useEffect(() => {

    }, []);

    return (

        <View style={styles.homeContainer}>
            <MenuBar {...props}/>
            <ModalChooseOrder {...props} modalVisible={modalVisible} closeModalVisible={closeModalChoose}
                              openModalOrder={openModalOrder}/>
            <ModalOrderRestaurantDetails {...props} modalOrder={modalOrder} closeModalOrder={closeModalOrder}
                                         openModalChoose={openModalChoose} openModalRecap={openModalRecap}/>
            <ModalReacapChoice {...props} openModalOrder={openModalOrder} modalRecap={modalRecap}
                               closeModalRecap={closeModalRecap} closeModalOrder={closeModalOrder}
                               closeModalVisible={closeModalChoose}/>


            <ScrollView style={{display: 'flex', flex: 1}}>
                <View style={{
                    backgroundColor: 'white',

                    justifyContent: 'center',
                    display: 'flex',
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                }}>
                    {props.point === null ?
                        <TouchableOpacity onPress={() => {
                            console.log('here start');
                            openModalChoose();
                        }}
                                          style={{
                                              alignItems: 'center',
                                              backgroundColor: '#E3002B',
                                              borderRadius: 50,
                                              padding: 16,
                                          }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >START MY ORDER</Text>
                        </TouchableOpacity>
                        :
                        <View style={{
                            backgroundColor: 'white',

                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }}>
                            <View style={{
                                display: 'flex',
                                flexDirection: 'column',
                                flex: 0.95,
                                justifyContent: 'center',
                            }}>
                                <Text style={{
                                    color: '#474545',
                                    fontSize: 12,

                                    fontFamily: 'Montserrat-Bold',
                                }}>Carryout from:</Text>
                                <Text style={{
                                    color: '#000000',
                                    fontSize: 14,
                                    fontFamily: 'Montserrat-Bold',
                                }}>{props.point?.name} at 7 pm</Text>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => {
                                    openModalChoose();
                                }} style={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    alignItems: 'center',

                                    backgroundColor: 'white',
                                }}

                                >
                                    <Text
                                        style={{
                                            color: '#E3002B',
                                            fontSize: 15,
                                            fontFamily: 'Montserrat-ExtraBold',
                                            marginRight:5

                                        }}

                                    >change</Text>
                                    <Next/>
                                </TouchableOpacity>

                            </View>

                        </View>
                    }
                </View>

                <View>
            <SubList {...props}/>
                </View>

            </ScrollView>

        </View>
    );
};
const mapStateToProps = state => {
    const {loading, indexed,orders,single,card,tot,nextt} = state.auth;
    const {check, menu, point} = state.appReducer;

    return {loading, check, menu, indexed, point,orders,single,card,tot,nextt};
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    resetOrder: () => dispatch(resetOrder()),
    savePoint: (col) => dispatch(savePoint(col)),
    goToSingle:(one,navigation)=>dispatch(goToSingle(one,navigation)),
    calculateTotal:(card)=>dispatch(calculateTotal(card)),
    getOne:()=>dispatch(getOne())
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
