import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, Image, ScrollView, FlatList, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../assets/styles';
import MenuBar from '../components/MenuBar';
import {
    calculateTotal,
    changeIndex, clearCard,
    getCollections,
    getMenu,
    getPaymentMethods, resetOrder, savePoint,
    saveProfileInformation,
    saveWay,
} from '../actions/data';
import {payOrder} from '../actions/action';

import Next from '../assets/images/next.svg';
import Down from '../assets/images/downr.svg';
import ModalChoosePayment from '../components/ModalChoosePayment';
import ChooseCollection from '../components/ModalChooseCollection';
import ModalUser from '../components/ModalUserDetails';
import ModalEat from '../components/ModalEat';
const RecapCheckout = (props) => {
    console.log('details cheout props', props);
    const [payment, setPayment] = useState(false);
    const [collection, setCollection] = useState(false);
    const [user, setUser] = useState(false);
    const [eat, setEat] = useState(false);
    const closeEat=()=>{
        setEat(false)
    }
    const openEat=()=>{
        setEat(true)
    }
    const closePayment = () => {
        setPayment(false);

    };
    const openPayment = () => {
        setPayment(true);
    };
    const closeCollection = () => {
        setCollection(false);
    };
    const openCollection = () => {
        setCollection(true);
    };
    const openUser = () => {
        setUser(true);
    };
    const closeUser = () => {
        setUser(false);
    };

    return (
        <View style={styles.checkoutContainer}>
            {eat &&
            <ModalEat {...props} eat={eat} closeEat={closeEat}closeCollection={closeCollection}  />
            }
            {payment &&
            <ModalChoosePayment {...props} payment={payment} closePayment={closePayment}/>}
            {collection &&
            <ChooseCollection {...props} collection={collection} closeCollection={closeCollection} openEat={openEat}/>}

            {user &&
            <ModalUser {...props} user={user} closeUser={closeUser}/>
            }
            <ScrollView style={{display: 'flex', flex: 1}}>


                <View style={styles.checkoutSection}>
                    <Text style={styles.TextColorExtended}>CHECKOUT</Text>

                </View>
                <View style={styles.btnSection}>
                </View>

                <TouchableOpacity style={styles.TouchableWithShadow}>
                    <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={styles.textCheckout}>COLLECTION TIME :</Text>
                        <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={styles.changeColor}>change</Text>
                            <Next/>
                        </View>

                    </View>
                    <Text style={styles.subTextCheckout}>7 pm</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => openUser()} style={styles.TouchableWithShadow}>
                    {props.profile == null ?
                        <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text style={styles.textCheckout}>Add your details</Text>
                            <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>

                                <Down/>
                            </View>

                        </View>
                        :
                        <View>
                            <View style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }}>
                                <Text style={{color: 'black', fontSize: 14, fontFamily: 'Montserrat-Bold'}}>CARRY OUT
                                    FOR {props.profile.firstName?.toUpperCase()}</Text>

                                <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                                    <Text style={styles.changeColor}>change</Text>
                                    <Next/>
                                </View>
                            </View>
                            <View style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between'}}>
                                <Text style={styles.textInfo}>{props.profile.firstName} {props.profile.lastName}</Text>
                                <Text>{props.profile.phoneNumber}</Text>
                                <Text>{props.profile.email}</Text>
                            </View>

                        </View>
                    }
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        openPayment();

                    }} style={styles.TouchableWithShadow}>
                    {!props.way ?
                        <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text style={styles.textCheckout}>add payment method</Text>
                            <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>

                                <Down/>
                            </View>

                        </View>
                        :
                        <View>
                            <View style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }}>
                                <Text style={{color: 'black', fontSize: 14, fontFamily: 'Montserrat-Bold'}}>PAYMENT METHOD</Text>

                                <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                                    <Text style={styles.changeColor}>change</Text>
                                    <Next/>
                                </View>
                            </View>
                            <View style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between'}}>
                                <Text style={{color: 'black', fontSize: 14, fontFamily: 'Montserrat-Bold',marginTop:10}}>{props.way.label?.toUpperCase()}</Text>
                                <Image source={props.way.image}/>

                            </View>

                        </View>
                    }
                </TouchableOpacity>
                <TouchableOpacity style={styles.TouchableWithShadow}>
                    {!props.point ?
                    <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={styles.textCheckout}>Choose a Collection Point</Text>
                        <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>

                            <Down/>
                        </View>

                    </View>
                        :
                        <View>
                            <View style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }}>
                                <Text style={{color: 'black', fontSize: 14, fontFamily: 'Montserrat-Bold'}}>COLLECT METHOD</Text>

                                <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                                    <Text style={styles.changeColor}>change</Text>
                                    <Next/>
                                </View>
                            </View>
                            <View style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between'}}>
                                <Text style={{color: 'black', fontSize: 14, fontFamily: 'Montserrat-Bold',marginBottom:5}}>{props.point.label?.toUpperCase()}</Text>
<Text>Come in store to collect your order it will be ready when you arrive.</Text>

                            </View>

                        </View>

                    }


                </TouchableOpacity>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingVertical: 50,
                }}>
                    <Text style={styles.TextColorExtended}>YOUR TOTAL ORDER : {(props.tot+1).toFixed(3)} DT</Text>
                </View>
                {props.profile !== null && props.way !== null &&
                <View>
                    {props.loading ?
                        <ActivityIndicator size="large" color="#E3002B"/>
                        :
                        <TouchableOpacity onPress={() => {
                            props.payOrder(props.card, props.navigation,props.tot)
                        }} style={{
                            alignItems: 'center',
                            backgroundColor: '#E3002B',
                            borderRadius: 50,
                            padding: 16,
                            marginVertical: 20,
                            marginHorizontal: 10,
                        }}

                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 15,
                                    fontFamily: 'Montserrat-Bold',
                                }}

                            >PURCHASE</Text>

                        </TouchableOpacity>
                    }
                </View>
                }
                <TouchableOpacity onPress={() => {
                    props.clearCard(props.navigation)

                }} style={{
                    alignItems: 'center',
                    backgroundColor: 'white',
                    borderRadius: 50,
                    padding: 16,
                    borderColor: 'black',
                    borderWidth: 1,
                    marginHorizontal: 10,
                    marginBottom:50
                }}

                >
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 15,
                            fontFamily: 'Montserrat-Bold',


                        }}

                    >CANCEL</Text>
                </TouchableOpacity>
            </ScrollView>

        </View>

    );

};
const mapStateToProps = state => {
    const {loading, indexed,orders,card,tot} = state.auth;
    const {check, menu, payments, collections, profile, way,point} = state.appReducer;

    return {loading, check, menu, indexed, payments, collections, profile, way,point,orders,card,tot};
};
const mapDispatchToProps = dispatch => ({
    getMenu: () => dispatch(getMenu()),
    changeIndex: (index) => dispatch(changeIndex(index)),
    getPaymentMethods: () => dispatch(getPaymentMethods()),
    getCollections: () => dispatch(getCollections()),
    saveProfileInformation: (payload) => dispatch(saveProfileInformation(payload)),
    saveWay: (pay, second) => dispatch(saveWay(pay, second)),
    savePoint:(col)=>dispatch(savePoint(col)),
    clearCard:(navigation)=>dispatch(clearCard(navigation)),
    payOrder:(purchases,navigation,tot)=>dispatch(payOrder(purchases,navigation,tot)),
    calculateTotal:(card)=>dispatch(calculateTotal(card)),
    resetOrder: () => dispatch(resetOrder()),

});

export default connect(mapStateToProps, mapDispatchToProps)(RecapCheckout);
