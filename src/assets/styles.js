import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


export default StyleSheet.create({

    homeContainer: {
        backgroundColor: '#FEF4ED',
        flex: 1,

    },
    checkoutContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },
    adventureView: {
        backgroundColor: 'white',
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 30,
    },
    dateAd: {
        color: '#E3002B',
        fontFamily: 'Montserrat-Bold',
        fontSize: 14,
        marginBottom: 5,

    },
    TextAbout: {
        color: '#000000',
        fontFamily: 'Montserrat-Bold',
        fontSize: 15,
        marginBottom: 15,

    },


    textAd: {
        color: '#717070',
        fontSize: 12,
        fontFamily: 'Montserrat-Medium',
        lineHeight: 20,
        marginBottom: 10,
    },
    adventureTitle: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'Montserrat-Black',

    },
    menuBarContainer: {
        backgroundColor: 'white',
        flex: 0.1,
        paddingLeft: 10,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {width: 10, height: 2},
        shadowOpacity: 1.5,
        shadowRadius: 2,
        elevation: 15,


    },
    closeDrawerView: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginHorizontal: 20,
        marginTop: 20,
    },
    threeTextContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginHorizontal: 20,
    },
    TextBorder: {},
    boxSimple: {
        backgroundColor: '#E3002B',
        borderWidth: 0.5,
        borderColor: '#E3002B',
        paddingVertical: 20,
        paddingHorizontal: 12,
        marginRight: 5,

    },
    MainContainerMeals: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: 30,
    },
    MainContainerMealsSub: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: 30,
    },
    imageThumbnail: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
    ListMealsButton: {

        display: 'flex',
        flexDirection: 'column',
        borderRadius: 6,
        borderWidth: 2,
        borderColor: 'white',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        paddingVertical: 20,
        marginLeft: 10,
        marginBottom: 15,
    },
    centeredView: {
        flex: 1,
        backgroundColor: 'white',
    },
    modalView: {
        marginTop: 40,
        marginBottom: 70,
        marginHorizontal: 20,
    },
    modalCollection: {
        display: 'flex',
        flex: 0.1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15,


    },
    modalUser: {
        display: 'flex',

        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 15,


    },
    openButton: {
        backgroundColor: '#F194FF',
        borderRadius: 20,
        padding: 10,
        elevation: 2,
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    },
    orderContainerBtn: {
        flex: 1,
        display: 'flex',

        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 4,
        flexDirection: 'column',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    btnContainerRecap: {
        backgroundColor: 'white',
        borderRadius: 4,
        padding: 20,
        marginVertical: 15,

        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    basketStyle: {
        flex: 0.4,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
    },
    textBasketStyle: {
        borderWidth: 2,
        borderColor: 'black',
        color: 'black',
        backgroundColor: 'white',
        padding: 5,
        fontSize: 13,
        textAlign: 'center',

        fontFamily: 'Montserrat-Bold',
        borderStartWidth: 2,
        borderEndWidth: 3,
        borderTopWidth: 1,

        borderRightWidth: 3,
        borderBottomWidth: 4,
    },
    textTotal: {
        color: 'black',
        fontSize: 14,
        paddingVertical: 12,
        paddingHorizontal: 5,
        fontFamily: 'Montserrat-Bold',
    },

    btnListDetails: {
        flex: 1,
        backgroundColor: 'white',
        marginHorizontal: 15,
        marginVertical: 5,
        flexDirection: 'row',


    },
    btnListService: {
        flex: 1,
        backgroundColor: 'white',
        marginHorizontal: 15,
        marginVertical: 5,
        flexDirection: 'row',


    },
    btnSand: {
        flex: 1,
        backgroundColor: 'white',
        marginHorizontal: 15,
        marginVertical: 5,
        flexDirection: 'row',
    },
    containerSingle: {
        flex: 1,
        backgroundColor: 'transparent',
        marginHorizontal: 15,
        marginVertical: 5,
        flexDirection: 'row',
    },
    containerContact: (selected) => ({
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 8,
        marginVertical: 8,
        borderRadius: 4,
        elevation: !selected ? 4 : 0,
        marginBottom: 10,
    }),
    containerSubContact: {
        display: 'flex',
        flexDirection: 'row',

        backgroundColor: 'white',
        padding: 10,
        marginHorizontal: 25,
        marginVertical: 8,
        borderRadius: 4,
        elevation: 4,
        marginBottom: 10,

    },
    firstList: {
        display: 'flex',
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between',
        flex: 1,
        alignItems: 'center',
    },
    listTwo: {
        backgroundColor: 'red',
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',


    },
    animation: {

        marginHorizontal: 17,

        backgroundColor: '#F7ECE5',
        width: '90%',
        borderRadius: 3,
        marginVertical: 1,

    },
    animationSingle: {

        backgroundColor: 'transparent',
        borderRadius: 3,
        marginVertical: 1,
    },
    paymentAnimation: {
        marginHorizontal: 17,

        backgroundColor: 'white',
        width: '90%',
        borderRadius: 3,
        marginVertical: 1,
    },
    textCheck: {
        color: '#949494',
        fontSize: 16,
        fontFamily: 'Montserrat-Medium',
        marginBottom: 15,
    },
    textCheckRight: {

        color: 'black',
        fontSize: 14,
        fontFamily: 'Montserrat-Bold',
        marginBottom: 15,

    },
    TouchableWithShadow: {
        display: 'flex',
        flexDirection: 'column',


        backgroundColor: 'white',
        padding: 25,
        marginHorizontal: 10,
        marginVertical: 8,
        borderRadius: 4,
        elevation: 4,
        shadowColor: 'grey',
        shadowOpacity: 0.5,
        shadowRadius: 10,
    },
    touchableCollection: {
        display: 'flex',
        flexDirection: 'column',


        backgroundColor: 'white',
        padding: 10,
        marginHorizontal: 10,
        marginVertical: 8,
        borderRadius: 4,
        elevation: 4,
        shadowColor: 'grey',
        shadowOpacity: 0.5,
        shadowRadius: 10,
    },
    checkoutSection: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 25,
    },
    btnSection: {
        display: 'flex',
        marginHorizontal: 20,
        backgroundColor: 'white',

        paddingHorizontal: 20,
    },
    changeColor: {
        color: '#E3002B',
        fontSize: 15,
        fontFamily: 'Montserrat-ExtraBold',
        marginHorizontal: 15,
    },
    textCheckout: {
        color: 'black',
        fontSize: 14,
        fontFamily: 'Montserrat-Bold',

    },
    subTextCheckout: {
        color: 'black',
        fontSize: 12,
        fontFamily: 'Montserrat-Bold',

    },
    TextColorExtended: {
        color: '#000',
        fontSize: 16,
        fontFamily: 'Montserrat-Black',
    },
    textInfo: {
        color: '#717070',
        fontFamily: 'Montserrat-Medium',
        fontSize: 13,
        marginBottom: 5,
    },
    changeView: active => ({

        display: 'flex',
        flex: 1,
        flexDirection: active ? 'column' : 'row',
        justifyContent: active ? 'center' : 'center',


    }),
    btnSandwich: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#ECEAEA',
        marginVertical: 5,
        backgroundColor: 'white',
        marginHorizontal: 10,
    },
    btnSubSandwich: {
        display: 'flex',
        flexDirection: 'row',
        padding: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 10,
    },
    btnTitleSup: {
        borderWidth: 1,
        borderColor: '#E3002B',
        alignItems: 'center',
        justifyContent: 'center',
        width: 32,
        height: 32,
        backgroundColor: '#E3002B',
        borderRadius: 50,
    },
    BtnContainerSandwich: {
        display: 'flex',
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between',
        flex: 1,
        alignItems: 'center',
    },
    btnDrawer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    textDrawer: {
        color: 'black',
        fontFamily: 'Montserrat-Black',
        fontSize: 12,
        marginLeft: 10,
    },
    btnDrawerItem: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 15,
        marginHorizontal: 15,
    },
    accountView: {
        display: 'flex',
        flexDirection: 'row',

        backgroundColor: 'white',
        padding: 25,
        marginHorizontal: 10,
        marginVertical: 8,
        borderRadius: 4,
        elevation: 4,
        marginBottom: 10,
    },

});
