// Imports: Dependencies
import React from 'react';
import {store,persistor} from './src/store/store';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import Container from './src/navigation/navigation';
// React Native App

export default function App() {
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>

                <Container/>
            </PersistGate>
        </Provider>

    );
}
